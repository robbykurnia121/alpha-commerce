function calcCountDown(toTime) {
  const now = new Date()
  if (now >= toTime)
    return [0, 0, 0]

  const second = (toTime.getTime() - now.getTime())/1000
  return [parseInt(second/3600), parseInt((second%3600)/60), parseInt(second % 60)]
}

export default calcCountDown