const IDNumberFormatter = Intl.NumberFormat('ID', {
  style: 'currency',
  currency: 'IDR',
})

const formatPrice = ({ price, currency="IDR" }) => {
  return IDNumberFormatter.format(
     Number.parseFloat(price).toFixed(0)
    ).replace(",00", "") 
}

export default formatPrice