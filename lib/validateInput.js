export const validateInput = (value, required, validation) => {
  let val = value    
  if (val === null || val === undefined)
    val = ""
  if (typeof val === 'string' || val instanceof String) {
    val = val.trim()
    if (required && val.length == 0)
      return false
    if (val.length > 0) {
      if (validation === 'email') {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(val).toLowerCase()))
          return false
      }
    }
  }
  
  return true
}

export default validateInput