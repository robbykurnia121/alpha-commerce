module.exports = {
  STORE_API_URL: process.env.GOAPP_STORE_API_URL,
  STORE_API_KEY: process.env.GOAPP_STORE_API_KEY,
  AUTH_ACCOUNT_URL: process.env.GOAPP_AUTH_ACCOUNT_URL,
  AUTH_API_URL: process.env.GOAPP_AUTH_API_URL,
  DIGITAL_STORE_URL: process.env.GOAPP_DIGITAL_STORE_URL,
  APP_TITLE: 'E-Commerce',
  SEO_TITLE: process.env.GOAPP_SEO_TITLE,
  SEO_DESCRIPTION: process.env.GOAPP_SEO_DESCRIPTION,
  SEO_SITE_NAME: process.env.GOAPP_SEO_SITE_NAME,
  SEO_URL: process.env.GOAPP_SEO_URL,
}