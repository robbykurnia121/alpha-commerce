import { Wallet, Folder, Shipping, Cubes } from '@components/icons'

module.exports = {
  ORDER_STATUSES: [
    {
      name: "Menunggu Pembayaran",
      icon: Wallet,
      isMainStatus: true,
      allowCancel: true,
      waitingForPayment: true,
      match: (order) => [
        "pending payment",
      ].includes(order.status.toLowerCase()),
    },
    {
      name: "Sedang Diproses",
      icon: Folder,
      isMainStatus: true,
      match: (order) => [
        "pending processing", 
        "waiting for processing",
        "pending items",
        "ready to pick",
        "waiting for items", 
        "ready to pack"
      ].includes(order.status.toLowerCase()),
    },
    {
      name: "Dikemas",
      icon: Cubes,      
      isMainStatus: true,
      match: (order) => [
        "ready to ship", "packing", "on packing"
      ].includes(order.status.toLowerCase()),
    },
    {
      name: "Sedang Dikirim",
      icon: Shipping,      
      allowReceiveItems: true,      
      isMainStatus: true,
      match: (order) => [
        "shipping", "delivered",
      ].includes(order.status.toLowerCase()),
    },
    {
      name: "Selesai",
      allowRateOrder: true,
      allowRequestReturn: true,      
      match: (order) => [
        "completed", "paid",
      ].includes(order.status.toLowerCase()),
    },
    {
      name: "Dibatalkan",
      match: (order) => order.completed_at && order.cancel_reason,
    },
    {
      name: "Pengembalian",
      match: (order) => order.return_order,
    },    
  ]
}
