export default [
  {
    key: "topup",
    name: "Pulsa",
    primary_image_url: "/images/digital-products/icon_pulsa.png",
    slug: "/pulsa/prabayar",
  },
  {
    key: "data",
    name: "Paket Data",
    primary_image_url: "/images/digital-products/icon_paketdata.png",
    slug: "/paket-data",
  },
  {
    key: "electricity",
    name: "Listrik",
    primary_image_url: "/images/digital-products/icon_listrik.png",
    slug: "/tagihan-listrik/prabayar",
  },
  {
    key: "game",
    name: "Voucher Game",
    primary_image_url: "/images/digital-products/icon_games.png",
    slug: "/voucher-game",
  },
];
