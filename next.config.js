module.exports = {
  reactStrictMode: true,
  env: {
    GOAPP_STORE_API_URL: process.env.GOAPP_STORE_API_URL,
    GOAPP_STORE_API_KEY: process.env.GOAPP_STORE_API_KEY,
    GOAPP_AUTH_API_URL: process.env.GOAPP_AUTH_API_URL,
    GOAPP_DIGITAL_STORE_URL: process.env.GOAPP_DIGITAL_STORE_URL,
  },
  images: {
    domains: ['media.dev.goapp.co.id', 'media.goapp.co.id'],
  },
  target: 'serverless',
};
