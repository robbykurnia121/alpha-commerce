import { get, put, post } from './request'

export const API_CART = "/sales/api/cart"
export const API_DISCOUNT_CODE = "/catalog/api/discount-code"

export const getCart = (cartID=0) => { 
  return get(`${API_CART}/${cartID}`)
}
export const updateCart = (cartID, data) => {
  return put(`${API_CART}/${cartID}`, data)
}
export const getPaymentMethods = (cartID=0) => { 
  return get(`${API_CART}/${cartID}/payment-methods`)
}
export const getShippingMethods = (cartID=0) => { 
  return get(`${API_CART}/${cartID}/shipping-methods`)
}
export const pay = (cartID, data) => {
  return post(`${API_CART}/${cartID}/pay`, data)
}
export const addDiscount = (cartID, code) => {
  return put(`${API_CART}/${cartID}/add_discount`, { code })
}
export const removeDiscount = (cartID, code) => {
  return put(`${API_CART}/${cartID}/remove_discount`, { code })
}
export const getDiscountCodeList = () => { 
  return get(`${API_DISCOUNT_CODE}`)
}
