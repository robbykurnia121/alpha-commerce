// import store from '../../store'
import http from './http'
import lo from 'lodash'
import fixUrlSuffix from '@lib/fixUrlSuffix'
import { STORE_API_KEY } from '@configs/keys'

export const request = (config, isProtected) => {
  // const { auth } = store.getState()
  // config.headers = { ...config.headers, "X-Service-Key": auth.userInfo.business_uid }
  // if (isProtected)
  //   config.headers = { ...config.headers, Authorization: `jwt ${auth.token}` }
  config.headers = { ...config.headers, "X-API-Key": STORE_API_KEY}

  let urlParams = {
    ...(config.urlParams || {})
  }
  if (typeof window !== 'undefined') {
    const visitor = window.alpha_store.getState().visitor?.registerResult?.visitor_id
    if (visitor)
      urlParams.visitor = visitor
    const auth = window.alpha_store.getState().auth
    if (auth.isLoggedIn) {
      const authorization = { 'Authorization': 'jwt ' + auth.authToken }
      config.headers = { ...config.headers, ...authorization }
    }
  }

  // Make sure url has suffix "/" as required by our backend
  let url = fixUrlSuffix(config.url)
  if (urlParams) {
    const urlStr = Object.keys(urlParams)
      .filter(k => !(urlParams[k] === null || urlParams[k] === undefined))
      .map(k => (
          encodeURIComponent(k) + "=" + encodeURIComponent(urlParams[k])
        )).join("&")
    if (urlStr) {
      if (url.indexOf("?") >= 0)
        url += "&" + urlStr
      else
        url += "?" + urlStr
    }
  }

  config = { ...config, url}
  
  return http.request(lo.pick(config, ['url', 'method', 'data', 'headers']))
}

export const get = async (url, opts={}, isProtected=true) => {
  const options = {
    method: 'get',
    url,
    urlParams: opts.urlParams,
    headers: opts.headers,
  }
  
  return request(options, isProtected)
}

export const post = async (url, body, opts={}, isProtected=true) => {
  const options = {
    method: 'post',
    url,
    urlParams: opts.urlParams,
    data: body,
    headers: opts.headers,
  }
  
  return request(options, isProtected)
}

export const patch = async (url, body, opts={}, isProtected=true) => {
  const options = {
    method: 'patch',
    url,
    urlParams: opts.urlParams,
    data: body,
    headers: opts.headers,
  }
  
  return request(options, isProtected)
}

export const put = async (url, body, opts={}, isProtected=true) => {
  const options = {
    method: 'put',
    url,
    urlParams: opts.urlParams,
    data: body,
    headers: opts.headers,
  }
  
  return request(options, isProtected)
}