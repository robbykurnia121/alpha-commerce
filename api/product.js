import { get, post } from './request';

export const API_PRODUCT = "/catalog/api/product";
export const API_FLASH_SALE = "/catalog/api/flash-sale";
export const API_PRODUCT_RATING = "/sales/api/rating";

export const getProductList = (filter, sort) => {
  const urlParams = {
    category: filter?.category || "",
    brand: filter?.brand || "",
    flash_sale: filter?.flash_sale || "",
    view: filter?.view || "",
    q: filter?.q || "",
    sort: sort || "",
    size: filter?.size || 20,
    offset: filter?.offset || 0,
  };

  // // console--.log(urlParams)

  return get(`${API_PRODUCT}/search`, { urlParams });
};
export const getProductDetail = (key) => get(`${API_PRODUCT}/${key}`);
export const getProductRating = (id) => get(`${API_PRODUCT_RATING}/?product=${id}`);
export const getFlashSaleList = () => {
  return get(`${API_FLASH_SALE}`);
};
