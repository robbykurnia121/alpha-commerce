import { get, patch, post } from './request'

export const API_CONTACT = "/contact/api/contact"

export const getContact = () => { 
  return get(`${API_CONTACT}/0`)
}
export const updateContact = (data) => { 
  return patch(`${API_CONTACT}/0`, data)
}