import { get } from './request'

export const API_BUSINESS = "/directory/api/business";

export const getBusinessDetail = () => get(`${API_BUSINESS}/0`);