import { get, put, post } from './request'

export const API_REFERRAL = "/member/api/member"

export const getReferralCode = () => {
  return get(`${API_REFERRAL}/0`)
}
