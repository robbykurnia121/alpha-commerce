import { get, post } from './request'

export const API_VISITOR = "/visitor/api"

export const init = (data) => post(`${API_VISITOR}/init`, data)
export const register = (data) => post(`${API_VISITOR}/register`, data)
export const logEvent = (data) => post(`${API_VISITOR}/log_event`, data)

export const getRecentProductView = () => { 
  const urlParams = { event_name: "view" }
  return get(`${API_VISITOR}/event/`, { urlParams })
}