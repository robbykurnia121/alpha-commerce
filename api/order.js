import { get, post, patch } from './request'

export const API_ORDER = "/sales/api/order"
export const API_RATING = "/sales/api/rating"
export const API_SHIPPING_ADDRESS = "/sales/api/shipping-address"

export const getOrderList = () => {
  const urlParams = {
  }

  return get(`${API_ORDER}`, { urlParams })
}
export const getOrderDetail = (key) => get(`${API_ORDER}/${key}`)
export const cancelOrder = (key, data) => {
  return post(`${API_ORDER}/${key}/cancel`, data)
}
export const receiveItems = (key, data) => {
  return post(`${API_ORDER}/${key}/receive-items`, data)
}
export const requestReturn = (key, data) => {
  return post(`${API_ORDER}/${key}/request-return`, data)
}
export const rateOrder = (key, data) => {
  return post(`${API_RATING}`, {
    order: { uid: key },
    ...data,
  })
}
export const getRatingsByOrder = (id) => {
  return get(`${API_RATING}/`, { urlParams: { order: id } })
}
export const uploadMedia = (type, data) => {
  return post(`/sales/api/${type}/upload_media`, data)
}
export const getShippingAddressList = () => get(`${API_SHIPPING_ADDRESS}`)
export const createShippingAddress = (data) => {
  return post(API_SHIPPING_ADDRESS, data)
}
export const updateShippingAddress = (key, data) => {
  return patch(`${API_SHIPPING_ADDRESS}/${key}`, data)
}
