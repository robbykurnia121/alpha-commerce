import { get, post } from './request'

export const API_CONVERSATION = "/conversation/api/conversation"
export const API_MESSAGE = "/conversation/api/message"

export const getConversationList = (offset=0, size=20, filters={}, order) => {
  let params = { offset, size }
  Object.keys(filters).forEach(k => {
  	params[k] = filters[k]
  })
  if (order)
  	params["order"] = order
  return get(`${API_CONVERSATION}`, {urlParams: params})
}
export const getConversationDetail = (key) => get(`${API_CONVERSATION}/${key}`)
export const getConversationMessages = (key, startFrom=0, size=20) => get(`${API_CONVERSATION}/${key}?message_size=${size}&message_start_from=${startFrom}`)
export const newConversation = (key) => post(`${API_CONVERSATION}/start`)
export const sendMessage = (key, text, files) => {
  let formData = new FormData()
  formData.append('body', text)
  if (files)
    files.map((file, index) => {
//      formData.append(`files${index}`, file)
      formData.append('attachments', file)
    })
  const urlParams = { conversation: key }
  return post(
    `${API_MESSAGE}/send`,
    formData,
    { urlParams }
  )
}

export const markReadAll = (key) => post(`${API_CONVERSATION}/${key}/read_all`)
export const startTyping = (key) => post(`${API_CONVERSATION}/${key}/start_typing`)


    
    