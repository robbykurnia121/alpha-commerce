import { post } from './authRequest'

export const AUTH_API_TOKEN_AUTH = "/token-auth"
export const AUTH_API_TOKEN_INFO = "/token-info"
export const AUTH_API_REGISTER = "/register"
export const AUTH_API_PROFILE = "/profile"

export const tokenAuthOTP = (email) => post(`${AUTH_API_TOKEN_AUTH}/otp`, { method: "email", address: email })
export const tokenAuth = (email, otp) => post(AUTH_API_TOKEN_AUTH, { username: email, otp_code: otp })
export const tokenInfo = (token) => post(AUTH_API_TOKEN_INFO, { token})
export const registerUserOTP = (email) => post(`${AUTH_API_REGISTER}/otp`, { method: "email", address: email })
export const registerUser = (email, otp, contact_data={}) => post(AUTH_API_REGISTER, { ...contact_data, email, otp_code: otp })
