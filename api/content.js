import { get, put, post } from './request'

export const API_CONTENT = "/content/api/content"

export const getHomeBannerList = () => {
  return get(`${API_CONTENT}/?model=banner&position_f=main&sort_by=-priority_group`)
}
