import { get, put } from './request'

export const API_ADDRESS = "/address/api"

export const searchSubdistricts = (q) => { 
  return get(`${API_ADDRESS}/subdistrict/`, { urlParams: { country: "ID", q }})
}