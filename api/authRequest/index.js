import http from './http'
import fixUrlSuffix from '@lib/fixUrlSuffix'
import { STORE_API_KEY } from '@configs/keys'
import lo from 'lodash'

// export const request = (config) => {
//   // Make sure url has suffix "/" as required by our backend
//   config = { ...config, url: fixUrlSuffix(config.url)}
  
//   //alert(config.method + " " + config.url)
  
//   return http.request(lo.pick(config, ['url', 'method', 'data', 'headers']))
// }

export const request = (config) => {
  config.headers = { ...config.headers, "X-API-Key": STORE_API_KEY}

  let urlParams = {
    ...(config.urlParams || {})
  }
  if (typeof window !== 'undefined') {
    const visitor = window.alpha_store.getState().visitor?.registerResult?.visitor_id
    if (visitor)
      urlParams.visitor = visitor
  }

  // Make sure url has suffix "/" as required by our backend
  let url = fixUrlSuffix(config.url)
  if (urlParams) {
    const urlStr = Object.keys(urlParams)
      .filter(k => !(urlParams[k] === null || urlParams[k] === undefined))
      .map(k => (
          encodeURIComponent(k) + "=" + encodeURIComponent(urlParams[k])
        )).join("&")
    if (urlStr) {
      if (url.indexOf("?") >= 0)
        url += "&" + urlStr
      else
        url += "?" + urlStr
    }
  }

  config = { ...config, url}
  
  return http.request(lo.pick(config, ['url', 'method', 'data', 'headers']))
}

export const post = async (url, body, opts={}) => {
  const options = {
    method: 'post',
    url: url,
    data: body,
    headers: opts.headers,
  }
  
  return request(options)
}
