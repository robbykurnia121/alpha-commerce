import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects';
import ls from 'local-storage';
import * as api from '@api';

// Action Types

export const VISITOR_ACTION_REGISTER = 'VISITOR_ACTION_REGISTER';
export const VISITOR_ACTION_LOG_EVENT = 'VISITOR_ACTION_LOG_EVENT';
export const VISITOR_ACTION_GET_RECENT_PRODUCT_VIEWS = 'VISITOR_ACTION_GET_RECENT_PRODUCT_VIEWS';

// Reducer Types

export const VISITOR_SET_INITIALIZING = 'VISITOR_SET_INITIALIZING';
export const VISITOR_SET_INIT_RESULT = 'VISITOR_SET_INIT_RESULT';
export const VISITOR_SET_REGISTERING = 'VISITOR_SET_REGISTERING';
export const VISITOR_SET_REGISTER_RESULT = 'VISITOR_SET_REGISTER_RESULT';
export const VISITOR_SET_FETCHING_RECENT_PRODUCT_VIEWS = 'VISITOR_SET_FETCHING_RECENT_PRODUCT_VIEWS';
export const VISITOR_SET_RECENT_PRODUCT_VIEWS = 'VISITOR_SET_RECENT_PRODUCT_VIEWS';

// Action

export const register = (param) => ({ type: VISITOR_ACTION_REGISTER, param });
export const logEvent = (eventName, eventData) => ({ type: VISITOR_ACTION_LOG_EVENT, eventName, eventData });
export const getRecentProductViews = () => ({ type: VISITOR_ACTION_GET_RECENT_PRODUCT_VIEWS });

// Sagas

function* registerSaga({ param = {} }) {
  yield put({ type: VISITOR_SET_INITIALIZING });

  let initData = {
    _referer: document.referrer,
    _origin: window.location.href
  };

  // Read persisted _goid
  let _goid = ls.get('_goid');
  if (_goid)
    initData["_goid"] = _goid;

  // console--.log("initData:", initData)
  const { ok, data: initResult } = yield call(api.init, initData);
  if (ok) {
    // console--.log("initResult:", initResult)
    yield put({ type: VISITOR_SET_INIT_RESULT, result: initResult });

    // Store _goid on local storage
    ls.set('_goid', initResult["_goid"]);

    yield put({ type: VISITOR_SET_REGISTERING });

    // Register
    let registerData = {
      _goid: initResult["_goid"],
      ...param
    };

    // console--.log("registerData:", registerData)
    const { ok, data: registerResult } = yield call(api.register, registerData);
    if (ok) {
      // console--.log("registerResult:", registerResult)
      yield put({ type: VISITOR_SET_REGISTER_RESULT, result: registerResult });
    }
    else
      yield put({ type: VISITOR_SET_REGISTERING, isRegistering: false });
  }
  else
    yield put({ type: VISITOR_SET_INITIALIZING, isInitializing: false });
}

function* logEventSaga({ eventName, eventData }) {
  const visitor = yield select(state => state.visitor);

  const logData = {
    _goid: visitor.initResult["_goid"],
    event_name: eventName,
    event_data: eventData,
  };

  yield call(api.logEvent, logData);
}

function* getRecentProductViewsSaga({ }) {
  // yield put({ type: VISITOR_SET_RECENT_PRODUCT_VIEWS })
  const { ok, data } = yield call(api.getRecentProductView);
  // console--.log("getRecentProductViewsSaga: ", ok, data)
  if (ok)
    yield put({ type: VISITOR_SET_RECENT_PRODUCT_VIEWS, data });
  else
    yield put({ type: VISITOR_SET_RECENT_PRODUCT_VIEWS, isFetching: false });
}

export function* visitorSaga() {
  yield takeLatest(VISITOR_ACTION_REGISTER, registerSaga);
  yield takeEvery(VISITOR_ACTION_LOG_EVENT, logEventSaga);
  yield takeLatest(VISITOR_ACTION_GET_RECENT_PRODUCT_VIEWS, getRecentProductViewsSaga);
}
