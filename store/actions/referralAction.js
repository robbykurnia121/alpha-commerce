import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects'
import * as api from '@api'

// Action Types

export const REFERRAL_ACTION_GET_CODE = 'REFERRAL_ACTION_SET_CODE'

// Reducer Types

export const REFERRAL_SET_CODE = 'REFERRAL_SET_CODE'

// Action

export const getReferralCode = () => ({ type: REFERRAL_ACTION_GET_CODE })

// Sagas

function* getReferralCodeSaga() {
  const { ok, data } = yield call(api.getReferralCode)
  if (ok)
    yield put({ type: REFERRAL_SET_CODE, referral: { ...data } })
  else
    yield put({ type: REFERRAL_SET_CODE, referral: null })
}

export function* referralSaga() {
  yield takeLatest(REFERRAL_ACTION_GET_CODE, getReferralCodeSaga)
}
