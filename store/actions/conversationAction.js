import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects';
import * as api from '@api';

// Constants
export const WS_STATUS = {
  Idle: "Idle",
  Connecting: "Connecting",
  Connected: "Connected",
  Disconnected: "Disconnected"
};

// Action Types

export const CONVERSATION_ACTION_WS_CONNECT = 'CONVERSATION_WS_CONNECT';
export const CONVERSATION_ACTION_WS_DISCONNECT = 'CONVERSATION_WS_DISCONNECT';
export const CONVERSATION_ACTION_WS_SEND = 'CONVERSATION_ACTION_WS_SEND';
// export const CONVERSATION_ACTION_GET_STATS = 'CONVERSATION_ACTION_GET_STATS'
export const CONVERSATION_ACTION_GET_CONVERSATION_LIST = 'CONVERSATION_ACTION_GET_CONVERSATION_LIST';
export const CONVERSATION_ACTION_GET_CONVERSATION_DETAIL = 'CONVERSATION_ACTION_GET_CONVERSATION_DETAIL';
export const CONVERSATION_ACTION_GET_CONVERSATION_MESSAGES = 'CONVERSATION_ACTION_GET_CONVERSATION_MESSAGES';
export const CONVERSATION_ACTION_NEW_CONVERSATION = 'CONVERSATION_ACTION_NEW_CONVERSATION';
export const CONVERSATION_ACTION_SEND_MESSAGE = 'CONVERSATION_ACTION_SEND_MESSAGE';
export const CONVERSATION_ACTION_MARK_READ_ALL = 'CONVERSATION_ACTION_MARK_READ_ALL';
export const CONVERSATION_ACTION_START_TYPING = 'CONVERSATION_ACTION_START_TYPING';

// Reducer Types

export const CONVERSATION_SET_WS_CONNECTING = 'CONVERSATION_SET_WS_CONNECT';
export const CONVERSATION_SET_WS_CONNECTED = 'CONVERSATION_SET_WS_CONNECTED';
export const CONVERSATION_SET_WS_DISCONNECTED = 'CONVERSATION_SET_WS_DISCONNECTED';
// export const CONVERSATION_SET_FETCHING_STATS = 'CONVERSATION_SET_FETCHING_STATS'
// export const CONVERSATION_SET_STATS = 'CONVERSATION_SET_STATS'
export const CONVERSATION_SET_FETCHING_CONVERSATION_LIST = 'CONVERSATION_SET_FETCHING_CONVERSATION_LIST';
export const CONVERSATION_SET_FETCHING_CONVERSATION_DETAIL = 'CONVERSATION_SET_FETCHING_CONVERSATION_DETAIL';
export const CONVERSATION_SET_FETCHING_CONVERSATION_MESSAGES = 'CONVERSATION_SET_FETCHING_CONVERSATION_MESSAGES';
export const CONVERSATION_SET_UPDATING_CONVERSATION = 'CONVERSATION_SET_UPDATING_CONVERSATION';
export const CONVERSATION_SET_CONVERSATION_LIST = 'CONVERSATION_SET_CONVERSATION_LIST';
export const CONVERSATION_SET_CONVERSATION_DETAIL = 'CONVERSATION_SET_CONVERSATION_DETAIL';
export const CONVERSATION_SET_CONVERSATION_MESSAGES = 'CONVERSATION_SET_CONVERSATION_MESSAGES';
export const CONVERSATION_SET_SENDING_MESSAGE = 'CONVERSATION_SET_SENDING_MESSAGE';
export const CONVERSATION_RECEIVE_SEND_MESSAGE_RESULT = 'CONVERSATION_RECEIVE_SEND_MESSAGE_RESULT';
export const CONVERSATION_RECEIVE_NEW_MESSAGE = 'CONVERSATION_RECEIVE_NEW_MESSAGE';
export const CONVERSATION_RECEIVE_CONVERSATION_UPDATE = 'CONVERSATION_RECEIVE_CONVERSATION_UPDATE';
export const CONVERSATION_RECEIVE_CONVERSATION_MESSAGES_UPDATE = 'CONVERSATION_RECEIVE_CONVERSATION_MESSAGES_UPDATE';

// Action

export const wsConnect = (host, online, chrome) => ({ type: CONVERSATION_ACTION_WS_CONNECT, host, online, chrome });
export const wsSend = (command, data) => ({ type: CONVERSATION_ACTION_WS_SEND, command, data });
export const getConversationStats = () => ({ type: CONVERSATION_ACTION_GET_STATS });
export const getConversationList = () => ({ type: CONVERSATION_ACTION_GET_CONVERSATION_LIST });
export const getConversationDetail = (key) => ({ type: CONVERSATION_ACTION_GET_CONVERSATION_DETAIL, key });
export const getConversationMessages = (key) => ({ type: CONVERSATION_ACTION_GET_CONVERSATION_MESSAGES, key });
export const newConversation = (completionHandler) => ({ type: CONVERSATION_ACTION_NEW_CONVERSATION, completionHandler });
export const sendMessage = (key, text, files) => ({ type: CONVERSATION_ACTION_SEND_MESSAGE, key, text, files });
export const markReadAll = (key) => ({ type: CONVERSATION_ACTION_MARK_READ_ALL, key });
export const startTyping = (key) => ({ type: CONVERSATION_ACTION_START_TYPING, key });

// Sagas

function* getConversationListSaga({ }) {
  yield put({ type: CONVERSATION_SET_FETCHING_CONVERSATION_LIST });
  const { ok, data } = yield call(api.getConversationList);
  if (ok)
    yield put({
      type: CONVERSATION_SET_CONVERSATION_LIST, data: {
        records: data,
      }
    });
  else
    yield put({ type: CONVERSATION_SET_FETCHING_CONVERSATION_LIST, isFetching: false });
}

function* getConversationDetailSaga({ key }) {
  yield put({ type: CONVERSATION_SET_FETCHING_CONVERSATION_DETAIL, key });
  const { ok, data } = yield call(api.getConversationDetail, key);
  if (ok)
    yield put({ type: CONVERSATION_SET_CONVERSATION_DETAIL, key, data });
  else
    yield put({ type: CONVERSATION_SET_FETCHING_CONVERSATION_DETAIL, key, isFetching: false });
}

function* getConversationMessagesSaga({ key }) {
  yield put({ type: CONVERSATION_SET_FETCHING_CONVERSATION_MESSAGES, key });

  const { ok, data } = yield call(api.getConversationMessages, key);

  if (ok)
    yield put({
      type: CONVERSATION_SET_CONVERSATION_MESSAGES,
      key,
      data: {
        total: data.messages.length,
        records: data.messages,
      },
    });
  else
    yield put({ type: CONVERSATION_SET_FETCHING_CONVERSATION_MESSAGES, key, isFetching: false });
}

function* newConversationSaga({ completionHandler }) {
  const { ok, data } = yield call(api.newConversation);
  // console--.log("newConversation:", ok, data)
  if (ok)
    yield put({
      type: CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
      key: data.uid,
      data,
    });

  if (completionHandler)
    completionHandler(ok, data);
}

function* sendMessageSaga({ key, text, files }) {
  if (!text || !text.trim())
    return;

  // Pending unique key
  const postKey = new Date().getTime();
  let initialData = {
    body: { text }
  };

  yield put({ type: CONVERSATION_SET_SENDING_MESSAGE, key, postKey, data: initialData });
  const { ok, data } = yield call(api.sendMessage, key, text);
  // console--.log("sendMessage", ok, data)
  if (ok)
    yield put({
      type: CONVERSATION_RECEIVE_SEND_MESSAGE_RESULT,
      key,
      postKey,
      data,
    });
  else
    yield put({ type: CONVERSATION_SET_SENDING_MESSAGE, key, postKey, isSending: false });
}

function* markReadAllSaga({ key }) {
  const { ok, data } = yield call(api.markReadAll, key);
}

function* startTypingSaga({ key }) {
  const { ok, data } = yield call(api.startTyping, key);
  if (ok)
    yield put({
      type: CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
      key: data.uid,
      data,
    });
}

export function* conversationSaga() {
  yield takeLatest(CONVERSATION_ACTION_GET_CONVERSATION_LIST, getConversationListSaga);
  yield takeLatest(CONVERSATION_ACTION_GET_CONVERSATION_DETAIL, getConversationDetailSaga);
  yield takeLatest(CONVERSATION_ACTION_GET_CONVERSATION_MESSAGES, getConversationMessagesSaga);
  yield takeEvery(CONVERSATION_ACTION_NEW_CONVERSATION, newConversationSaga);
  yield takeEvery(CONVERSATION_ACTION_SEND_MESSAGE, sendMessageSaga);
  yield takeEvery(CONVERSATION_ACTION_MARK_READ_ALL, markReadAllSaga);
  yield takeEvery(CONVERSATION_ACTION_START_TYPING, startTypingSaga);
}
