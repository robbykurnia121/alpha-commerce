import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects';
import ls from 'local-storage';
import * as api from '@api';

// Action Types

export const AUTH_ACTION_INIT = 'AUTH_ACTION_INIT';
export const AUTH_ACTION_LOGIN_GET_OTP = 'AUTH_ACTION_LOGIN_GET_OTP';
export const AUTH_ACTION_LOGIN_WITH_OTP = 'AUTH_ACTION_LOGIN_WITH_OTP';
export const AUTH_ACTION_REGISTER_GET_OTP = 'AUTH_ACTION_REGISTER_GET_OTP';
export const AUTH_ACTION_REGISTER_WITH_OTP = 'AUTH_ACTION_REGISTER_WITH_OTP';
export const AUTH_ACTION_LOGOUT = 'AUTH_ACTION_LOGOUT';

// Reducer Types

export const AUTH_SET_INITIALIZING = 'AUTH_SET_INITIALIZING';
export const AUTH_SET_NOT_LOGGED_IN = 'AUTH_SET_NOT_LOGGED_IN';
export const AUTH_SET_USER_INFO = 'AUTH_SET_USER_INFO';
export const AUTH_SET_TRIGGER_PROFILE = 'AUTH_SET_TRIGGER_PROFILE';

// Action

export const authInit = () => ({ type: AUTH_ACTION_INIT });
export const getLoginOTP = (email, completionHandler) => ({ type: AUTH_ACTION_LOGIN_GET_OTP, email, completionHandler });
export const loginWithOTP = (email, otp, completionHandler) => ({ type: AUTH_ACTION_LOGIN_WITH_OTP, email, otp, completionHandler });
export const getRegisterOTP = (email, completionHandler) => ({ type: AUTH_ACTION_REGISTER_GET_OTP, email, completionHandler });
export const registerWithOTP = (email, otp, contact_data, completionHandler) => ({ type: AUTH_ACTION_REGISTER_WITH_OTP, email, otp, contact_data, completionHandler });
export const logout = () => ({ type: AUTH_ACTION_LOGOUT });

// Sagas

function* initSaga({ }) {
  yield put({ type: AUTH_SET_INITIALIZING });

  // Read persisted _goid
  let authToken = ls.get('_authToken');
  if (authToken) {
    const { ok, data: userInfo } = yield call(api.tokenInfo, authToken);
    // console--.log("tokenInfo:", ok, userInfo)

    if (ok) {
      yield put({ type: AUTH_SET_USER_INFO, authToken, userInfo });
      return;
    }
  }

  yield put({ type: AUTH_SET_NOT_LOGGED_IN });
}

function* getLoginOTPSaga({ email, completionHandler }) {
  const { ok, data, status } = yield call(api.tokenAuthOTP, email);
  // console--.log("getLoginOTPSaga:", ok, data, status)

  completionHandler(ok, data, status);
}

function* loginWithOTPSaga({ email, otp, completionHandler }) {
  const { ok, data } = yield call(api.tokenAuth, email, otp);
  // console--.log("getLoginOTPSaga:", ok, data)
  if (ok) {
    const authToken = data.token;

    const { ok, data: userInfo } = yield call(api.tokenInfo, authToken);
    // console--.log("tokenInfo:", ok, userInfo)

    // Store token on local storage
    ls.set('_authToken', authToken);

    yield put({ type: AUTH_SET_USER_INFO, userInfo, authToken });

    if (completionHandler)
      completionHandler(ok, userInfo);

    // if (userInfo?.first_name === '' && userInfo?.last_name === '') {
    //   yield put({ type: AUTH_SET_TRIGGER_PROFILE, triggerProfileModal: true })
    // }
  }
  else {
    if (completionHandler)
      completionHandler(ok, data);
  }
}

function* getRegisterOTPSaga({ email, completionHandler }) {
  const { ok, data } = yield call(api.registerUserOTP, email);
  // console--.log("getRegisterOTPSaga:", ok, data)

  completionHandler(ok, data);
}

function* registerWithOTPSaga({ email, otp, contact_data, completionHandler }) {
  const { ok, data } = yield call(api.registerUser, email, otp, contact_data);
  // console--.log("registerWithOTPSaga:", ok, data)
  if (ok) {
    const authToken = data.token;

    const { ok, data: userInfo } = yield call(api.tokenInfo, authToken);
    // console--.log("tokenInfo:", ok, userInfo)

    // Store token on local storage
    ls.set('_authToken', authToken);

    yield put({ type: AUTH_SET_USER_INFO, userInfo, authToken });

    if (completionHandler)
      completionHandler(ok, userInfo);

    // if (userInfo?.first_name === '' && userInfo?.last_name === '') {
    //   yield put({ type: AUTH_SET_TRIGGER_PROFILE, triggerProfileModal: true })
    // }
  }
  else {
    if (completionHandler)
      completionHandler(ok, data);
  }
}

function* logoutSaga({ }) {
  ls.remove('_authToken');
  yield put({ type: AUTH_SET_NOT_LOGGED_IN });
}


export function* authSaga() {
  yield takeEvery(AUTH_ACTION_INIT, initSaga);
  yield takeEvery(AUTH_ACTION_LOGIN_GET_OTP, getLoginOTPSaga);
  yield takeEvery(AUTH_ACTION_LOGIN_WITH_OTP, loginWithOTPSaga);
  yield takeEvery(AUTH_ACTION_REGISTER_GET_OTP, getRegisterOTPSaga);
  yield takeEvery(AUTH_ACTION_REGISTER_WITH_OTP, registerWithOTPSaga);
  yield takeEvery(AUTH_ACTION_LOGOUT, logoutSaga);
}
