import { call, takeLatest, put } from 'redux-saga/effects';
import * as api from '@api'

// Action Types

export const BUSINESS_ACTION_GET_DETAIL = 'BUSINESS_ACTION_GET_DETAIL';

// Business Reducer Types

export const BUSINESS_SET_DETAIL = 'BUSINESS_SET_DETAIL';



export const getBusinessDetail = () => {
  return ({ type: BUSINESS_ACTION_GET_DETAIL });
}

function* getBusinessDetailSaga() {
  const { ok, data } = yield call(api.getBusinessDetail);
  if (ok)
    yield put({ type: BUSINESS_SET_DETAIL, data });
}

export function* businessSaga() {
  yield takeLatest(BUSINESS_ACTION_GET_DETAIL, getBusinessDetailSaga);
}
