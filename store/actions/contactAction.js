import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects';
import * as api from '@api';

// Action Types

export const CONTACT_ACTION_GET_CONTACT = 'CONTACT_ACTION_GET_CONTACT';
export const CONTACT_ACTION_UPDATE_CONTACT = 'CONTACT_ACTION_UPDATE_CONTACT';

// Reducer Types

export const CONTACT_SET_FETCHING_CONTACT = 'CONTACT_SET_FETCHING_CONTACT';
export const CONTACT_SET_CONTACT = 'CONTACT_SET_CONTACT';
export const CONTACT_SET_UPDATING_CONTACT = 'CONTACT_SET_UPDATING_CONTACT';

// Action

export const getContact = () => ({ type: CONTACT_ACTION_GET_CONTACT });
export const updateContact = (data, completionHandler) => ({ type: CONTACT_ACTION_UPDATE_CONTACT, data, completionHandler });

// Sagas

function* getContactSaga() {
  yield put({ type: CONTACT_SET_FETCHING_CONTACT });
  const { ok, data } = yield call(api.getContact);
  // console--.log("getContact:", ok, data)
  if (ok)
    yield put({ type: CONTACT_SET_CONTACT, data });
  else
    yield put({ type: CONTACT_SET_CONTACT, data: {} });
}

function* updateContactSaga({ data, completionHandler }) {
  yield put({ type: CONTACT_SET_UPDATING_CONTACT });
  const { ok, data: cartData } = yield call(api.updateContact, data);
  if (ok)
    yield put({ type: CONTACT_SET_CONTACT, data: cartData });
  else
    yield put({ type: CONTACT_SET_UPDATING_CONTACT, isUpdating: false });

  if (completionHandler)
    completionHandler(ok, data);
}

export function* contactSaga() {
  yield takeLatest(CONTACT_ACTION_GET_CONTACT, getContactSaga);
  yield takeEvery(CONTACT_ACTION_UPDATE_CONTACT, updateContactSaga);
}
