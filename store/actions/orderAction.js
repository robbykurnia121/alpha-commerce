import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects';
import * as api from '@api';

// Action Types

export const ORDER_ACTION_GET_ORDER_LIST = 'ORDER_ACTION_GET_ORDER_LIST';
export const ORDER_ACTION_GET_ORDER_DETAIL = 'ORDER_ACTION_GET_ORDER_DETAIL';
export const ORDER_ACTION_CANCEL_ORDER = 'ORDER_ACTION_CANCEL_ORDER';
export const ORDER_ACTION_RECEIVE_ITEMS = 'ORDER_ACTION_RECEIVE_ITEMS';
export const ORDER_ACTION_REQUEST_RETURN = 'ORDER_ACTION_REQUEST_RETURN';
export const ORDER_ACTION_RATE_ORDER = 'ORDER_ACTION_RATE_ORDER';
export const ORDER_ACTION_GET_SHIPPING_ADDRESS_LIST = 'ORDER_ACTION_GET_SHIPPING_ADDRESS_LIST';
export const ORDER_ACTION_CREATE_SHIPPING_ADDRESS = 'ORDER_ACTION_CREATE_SHIPPING_ADDRESS';
export const ORDER_ACTION_UPDATE_SHIPPING_ADDRESS = 'ORDER_ACTION_UPDATE_SHIPPING_ADDRESS';
export const ORDER_ACTION_GET_RATINGS_BY_ORDER = 'ORDER_ACTION_GET_RATINGS_BY_ORDER';

// Reducer Types

export const ORDER_SET_FETCHING_ORDER_LIST = 'ORDER_SET_FETCHING_ORDER_LIST';
export const ORDER_SET_ORDER_LIST = 'ORDER_SET_ORDER_LIST';
export const ORDER_SET_FETCHING_ORDER_DETAIL = 'ORDER_SET_FETCHING_ORDER_DETAIL';
export const ORDER_SET_UPDATING_ORDER = 'ORDER_SET_UPDATING_ORDER';
export const ORDER_SET_ORDER_DETAIL = 'ORDER_SET_ORDER_DETAIL';
export const ORDER_SET_FETCHING_SHIPPING_ADDRESS_LIST = 'ORDER_SET_FETCHING_SHIPPING_ADDRESS_LIST';
export const ORDER_SET_SHIPPING_ADDRESS_LIST = 'ORDER_SET_SHIPPING_ADDRESS_LIST';

// Action

export const getOrderList = (filter) => ({ type: ORDER_ACTION_GET_ORDER_LIST, filter });
export const getOrderDetail = (key) => ({ type: ORDER_ACTION_GET_ORDER_DETAIL, key });
export const cancelOrder = (key, data, completionHandler) => ({ type: ORDER_ACTION_CANCEL_ORDER, key, data, completionHandler });
export const receiveItems = (key, completionHandler) => ({ type: ORDER_ACTION_RECEIVE_ITEMS, key, completionHandler });
export const requestReturn = (key, data, completionHandler) => ({ type: ORDER_ACTION_REQUEST_RETURN, key, data, completionHandler });
export const rateOrder = (key, data, completionHandler) => ({ type: ORDER_ACTION_RATE_ORDER, key, data, completionHandler });
export const getShippingAddressList = () => ({ type: ORDER_ACTION_GET_SHIPPING_ADDRESS_LIST });
export const createShippingAddress = (data, completionHandler) => ({ type: ORDER_ACTION_CREATE_SHIPPING_ADDRESS, data, completionHandler });
export const updateShippingAddress = (key, data, completionHandler) => ({ type: ORDER_ACTION_UPDATE_SHIPPING_ADDRESS, key, data, completionHandler });
export const getRatingsByOrder = (id, completionHandler) => ({ type: ORDER_ACTION_GET_RATINGS_BY_ORDER, id, completionHandler });

// Sagas

function* getOrderListSaga({ filter = {}, sort = null }) {
  // // console--.log("getOrderList")  
  yield put({ type: ORDER_SET_FETCHING_ORDER_LIST, filter });
  const { ok, data } = yield call(api.getOrderList, filter);
  // // console--.log("getOrderList: ", data)
  if (ok)
    yield put({ type: ORDER_SET_ORDER_LIST, filter, data });
  else
    yield put({ type: ORDER_SET_FETCHING_ORDER_LIST, filter, isFetching: false });
}

function* getOrderDetailSaga({ key }) {
  yield put({ type: ORDER_SET_FETCHING_ORDER_DETAIL, key });
  const { ok, data } = yield call(api.getOrderDetail, key);
  if (ok)
    yield put({ type: ORDER_SET_ORDER_DETAIL, key, data });
  else
    yield put({ type: ORDER_SET_FETCHING_ORDER_DETAIL, key, isFetching: false });
}

function* cancelOrderSaga({ key, data, completionHandler }) {
  yield put({ type: ORDER_SET_UPDATING_ORDER, key });
  const { ok, data: result } = yield call(api.cancelOrder, key, data);
  // console--.log("cancelOrder Result:", ok, key, result)
  if (ok) {
    yield put({ type: ORDER_SET_ORDER_DETAIL, key, data: result });
  }
  else
    yield put({ type: ORDER_SET_UPDATING_ORDER, isUpdating: false });

  if (completionHandler)
    completionHandler(ok, result);
}

function* receiveItemsSaga({ key, completionHandler }) {
  yield put({ type: ORDER_SET_UPDATING_ORDER, key });
  const { ok, data: result } = yield call(api.receiveItems, key);
  // console--.log("receiveItems Result:", ok, key, result)
  if (ok) {
    yield put({ type: ORDER_SET_ORDER_DETAIL, key, data: result });
  }
  else
    yield put({ type: ORDER_SET_UPDATING_ORDER, isUpdating: false });

  if (completionHandler)
    completionHandler(ok, result);
}

function* requestReturnSaga({ key, data, completionHandler }) {
  yield put({ type: ORDER_SET_UPDATING_ORDER, key });
  const { ok, data: result } = yield call(api.requestReturn, key, data);
  // console--.log("requestReturn Result:", ok, result)
  if (ok) {
    yield put({ type: ORDER_SET_ORDER_DETAIL, key, data: result });
  }
  else
    yield put({ type: ORDER_SET_UPDATING_ORDER, isUpdating: false });

  if (completionHandler)
    completionHandler(ok, result);
}

function* rateOrderSaga({ key, data, completionHandler }) {
  yield put({ type: ORDER_SET_UPDATING_ORDER, key });
  const { ok } = yield call(api.rateOrder, key, data);
  // console--.log("rateOrder Result:", ok)

  if (ok) {
    // Rate order return rating data. Refetch order detail to get rating detail
    const { ok, data: result } = yield call(api.getOrderDetail, key);
    if (ok)
      yield put({ type: ORDER_SET_ORDER_DETAIL, key, data: result });
    else
      yield put({ type: ORDER_SET_UPDATING_ORDER, isUpdating: false });
  }
  else
    yield put({ type: ORDER_SET_UPDATING_ORDER, isUpdating: false });

  if (completionHandler)
    completionHandler(ok);
}

function* getShippingAddressListSaga({ }) {
  yield put({ type: ORDER_SET_FETCHING_SHIPPING_ADDRESS_LIST });
  const { ok, data } = yield call(api.getShippingAddressList);
  // console--.log("getShippingAddressListSaga: ", data)
  if (ok)
    yield put({ type: ORDER_SET_SHIPPING_ADDRESS_LIST, data });
  else
    yield put({ type: ORDER_SET_FETCHING_SHIPPING_ADDRESS_LIST, isFetching: false });
}

function* createShippingAddressSaga({ data, completionHandler }) {
  const { ok, data: result } = yield call(api.createShippingAddress, data);
  // console--.log("createShippingAddressSaga Result:", ok, result)

  if (completionHandler)
    completionHandler(ok, result);
}

function* updateShippingAddressSaga({ key, data, completionHandler }) {
  const { ok, data: result } = yield call(api.updateShippingAddress, key, data);
  // console--.log("updateShippingAddressSaga Result:", ok, result)

  if (completionHandler)
    completionHandler(ok, result);
}

function* getRatingsByOrderSaga({ id, completionHandler }) {
  const { ok, data } = yield call(api.getRatingsByOrder, id);

  if (completionHandler)
    completionHandler(ok, data);
}

export function* orderSaga() {
  yield takeLatest(ORDER_ACTION_GET_ORDER_LIST, getOrderListSaga);
  yield takeLatest(ORDER_ACTION_GET_ORDER_DETAIL, getOrderDetailSaga);
  yield takeEvery(ORDER_ACTION_CANCEL_ORDER, cancelOrderSaga);
  yield takeEvery(ORDER_ACTION_RECEIVE_ITEMS, receiveItemsSaga);
  yield takeEvery(ORDER_ACTION_REQUEST_RETURN, requestReturnSaga);
  yield takeEvery(ORDER_ACTION_RATE_ORDER, rateOrderSaga);
  yield takeLatest(ORDER_ACTION_GET_SHIPPING_ADDRESS_LIST, getShippingAddressListSaga);
  yield takeEvery(ORDER_ACTION_CREATE_SHIPPING_ADDRESS, createShippingAddressSaga);
  yield takeEvery(ORDER_ACTION_UPDATE_SHIPPING_ADDRESS, updateShippingAddressSaga);
  yield takeLatest(ORDER_ACTION_GET_RATINGS_BY_ORDER, getRatingsByOrderSaga);
}
