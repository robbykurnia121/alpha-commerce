import { visitorSaga } from './visitorAction'
import { productSaga } from './productAction'
import { cartSaga } from './cartAction'
import { orderSaga } from './orderAction'
import { authSaga } from './authAction'
import { businessSaga } from './businessAction';
import { contactSaga } from './contactAction'
import { conversationSaga } from './conversationAction'
import { referralSaga } from './referralAction'
import { contentSaga } from './contentActions'
import { spawn, all, call } from 'redux-saga/effects'

export default function* rootSaga() {
  // With spawn, we can't use saga.toPromise to wait until all saga ended
  // yield spawn(productSaga)
  yield all([
    call(visitorSaga),
    call(authSaga),
    call(businessSaga),
    call(productSaga),
    call(cartSaga),
    call(orderSaga),
    call(contactSaga),
    call(conversationSaga),
    call(referralSaga),
    call(contentSaga)
  ])
}
