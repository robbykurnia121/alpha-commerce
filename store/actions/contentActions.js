import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects'
import * as api from '@api'

// Action Types

export const CONTENT_ACTION_GET_BANNER = 'CONTENT_ACTION_GET_BANNER'

// Reducer Types

export const CONTENT_SET_BANNER = 'CONTENT_SET_BANNER'

// Action

export const getHomeBannerList = () => ({ type: CONTENT_ACTION_GET_BANNER })

// Sagas

function* getHomeBannerSaga() {
  const { ok, data } = yield call(api.getHomeBannerList)
  if (ok)
    yield put({ type: CONTENT_SET_BANNER, banner: data })
}

export function* contentSaga() {
  yield takeEvery(CONTENT_ACTION_GET_BANNER, getHomeBannerSaga)
}
