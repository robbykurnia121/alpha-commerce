import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects';
import * as api from '@api';

// Action Types

export const PRODUCT_ACTION_GET_PRODUCT_LIST = 'PRODUCT_ACTION_GET_PRODUCT_LIST';
export const PRODUCT_ACTION_GET_PRODUCT_DETAIL = 'PRODUCT_ACTION_GET_PRODUCT_DETAIL';
export const PRODUCT_ACTION_GET_PRODUCT_RATING = 'PRODUCT_ACTION_GET_PRODUCT_RATING';
export const PRODUCT_ACTION_GET_FLASH_SALE_LIST = 'PRODUCT_ACTION_GET_FLASH_SALE_LIST';

// Reducer Types

export const PRODUCT_SET_FETCHING_PRODUCT_LIST = 'PRODUCT_SET_FETCHING_PRODUCT_LIST';
export const PRODUCT_SET_PRODUCT_LIST = 'PRODUCT_SET_PRODUCT_LIST';
export const PRODUCT_SET_FETCHING_PRODUCT_DETAIL = 'PRODUCT_SET_FETCHING_PRODUCT_DETAIL';
export const PRODUCT_SET_PRODUCT_DETAIL = 'PRODUCT_SET_PRODUCT_DETAIL';
export const PRODUCT_SET_PRODUCT_RATING = 'PRODUCT_SET_PRODUCT_RATING';
export const PRODUCT_SET_FETCHING_FLASH_SALE_LIST = 'PRODUCT_SET_FETCHING_FLASH_SALE_LIST';
export const PRODUCT_SET_FLASH_SALE_LIST = 'PRODUCT_SET_FLASH_SALE_LIST';

// Action

export const getProductList = (filter, sort = "popularity") => ({ type: PRODUCT_ACTION_GET_PRODUCT_LIST, filter, sort });
export const getProductDetail = (key) => ({ type: PRODUCT_ACTION_GET_PRODUCT_DETAIL, key });
export const getProductRating = (id) => ({ type: PRODUCT_ACTION_GET_PRODUCT_RATING, id });
export const getFlashSaleList = () => ({ type: PRODUCT_ACTION_GET_FLASH_SALE_LIST });

// Sagas

function* getProductListSaga({ filter = {}, sort = null }) {
  yield put({ type: PRODUCT_SET_FETCHING_PRODUCT_LIST, filter, sort });
  const { ok, data } = yield call(api.getProductList, filter, sort);
  if (ok)
    yield put({ type: PRODUCT_SET_PRODUCT_LIST, filter, sort, data });
  else
    yield put({ type: PRODUCT_SET_FETCHING_PRODUCT_LIST, filter, sort, isFetching: false });
}

function* getProductDetailSaga({ key }) {
  yield put({ type: PRODUCT_SET_FETCHING_PRODUCT_DETAIL, key });
  const { ok, data } = yield call(api.getProductDetail, key);
  if (ok)
    yield put({ type: PRODUCT_SET_PRODUCT_DETAIL, key, data });
  else
    yield put({ type: PRODUCT_SET_FETCHING_PRODUCT_DETAIL, key, isFetching: false });
}

function* getProductRatingSaga({ id }) {
  const { ok, data } = yield call(api.getProductRating, id);
  if (ok)
    yield put({ type: PRODUCT_SET_PRODUCT_RATING, data });
  else
    yield put({ type: PRODUCT_SET_PRODUCT_RATING, data: [] });
}

function* getFlashSaleListSaga({ }) {
  yield put({ type: PRODUCT_SET_FETCHING_FLASH_SALE_LIST });
  const { ok, data } = yield call(api.getFlashSaleList);
  // // console--.log("getFlashSaleListSaga:", ok, data)
  if (ok)
    yield put({
      type: PRODUCT_SET_FLASH_SALE_LIST, data: {
        records: data
      }
    });
  else
    yield put({ type: PRODUCT_SET_FETCHING_FLASH_SALE_LIST, isFetching: false });
}

export function* productSaga() {
  yield takeEvery(PRODUCT_ACTION_GET_PRODUCT_LIST, getProductListSaga);
  yield takeLatest(PRODUCT_ACTION_GET_PRODUCT_DETAIL, getProductDetailSaga);
  yield takeLatest(PRODUCT_ACTION_GET_FLASH_SALE_LIST, getFlashSaleListSaga);
  yield takeLatest(PRODUCT_ACTION_GET_PRODUCT_RATING, getProductRatingSaga);
}
