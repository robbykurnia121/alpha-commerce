import { call, put, fork, select, takeLatest, takeEvery } from 'redux-saga/effects'
import * as api from '@api'
import { toast } from 'react-toastify'

// Action Types

export const CART_ACTION_GET_CART = 'CART_ACTION_GET_CART'
export const CART_ACTION_UPDATE_CART = 'CART_ACTION_UPDATE_CART'
export const CART_ACTION_ADD_TO_CART = 'CART_ACTION_ADD_TO_CART'
export const CART_ACTION_REMOVE_FROM_CART = 'CART_ACTION_REMOVE_FROM_CART'
export const CART_ACTION_SEARCH_SUBDISTRICTS = 'CART_ACTION_SEARCH_SUBDISTRICTS'
export const CART_ACTION_GET_PAYMENT_METHODS = 'CART_ACTION_GET_PAYMENT_METHODS'
export const CART_ACTION_GET_SHIPPING_METHODS = 'CART_ACTION_GET_SHIPPING_METHODS'
export const CART_ACTION_UPDATE_CONTACT = 'CART_ACTION_UPDATE_CONTACT'
export const CART_ACTION_UPDATE_SHIP_TO = 'CART_ACTION_UPDATE_SHIP_TO'
export const CART_ACTION_UPDATE_SHIPPING_METHOD = 'CART_ACTION_UPDATE_SHIPPING_METHOD'
export const CART_ACTION_UPDATE_PAYMENT_METHOD = 'CART_ACTION_UPDATE_PAYMENT_METHOD'
export const CART_ACTION_PAY = 'CART_ACTION_PAY'
export const CART_ACTION_ADD_DISCOUNT = 'CART_ACTION_ADD_DISCOUNT'
export const CART_ACTION_REMOVE_DISCOUNT = 'CART_ACTION_REMOVE_DISCOUNT'
export const CART_ACTION_GET_DISCOUNT_CODES = 'CART_ACTION_GET_DISCOUNT_CODES'

// Reducer Types

export const CART_SET_FETCHING_CART = 'CART_SET_FETCHING_CART'
export const CART_SET_CART = 'CART_SET_CART'
export const CART_SET_UPDATING_CART = 'CART_SET_UPDATING_CART'

// Action

export const getCart = () => ({ type: CART_ACTION_GET_CART })
export const updateCart = (data) => ({ type: CART_ACTION_UPDATE_CART, data })
export const addToCart = (product, quantity=1, message) => ({ type: CART_ACTION_ADD_TO_CART, product, quantity, message})
export const removeFromCart = (product, quantity=1, message) => ({ type: CART_ACTION_REMOVE_FROM_CART, product, quantity, message})
export const searchSubdistricts = (q, completionHandler) => ({ type: CART_ACTION_SEARCH_SUBDISTRICTS, q, completionHandler })
export const getPaymentMethods = (completionHandler) => ({ type: CART_ACTION_GET_PAYMENT_METHODS, completionHandler })
export const getShippingMethods = (completionHandler) => ({ type: CART_ACTION_GET_SHIPPING_METHODS, completionHandler })
export const updateCartContact = (contact_data) => ({ type: CART_ACTION_UPDATE_CONTACT, contact_data })
export const updateShipTo = (ship_to, completionHandler) => ({ type: CART_ACTION_UPDATE_SHIP_TO, ship_to, completionHandler })
export const updateShippingMethod = (shipping_method, message) => ({ type: CART_ACTION_UPDATE_SHIPPING_METHOD, shipping_method, message })
export const updatePaymentMethod = (payment_method, message) => ({ type: CART_ACTION_UPDATE_PAYMENT_METHOD, payment_method, message })
export const addDiscount = (code, completionHandler) => ({ type: CART_ACTION_ADD_DISCOUNT, code, completionHandler })
export const removeDiscount = (code, completionHandler) => ({ type: CART_ACTION_REMOVE_DISCOUNT, code, completionHandler })
export const pay = (callback_url, completionHandler) => ({ type: CART_ACTION_PAY, callback_url, completionHandler })
export const getDiscountCodes = (completionHandler) => ({ type: CART_ACTION_GET_DISCOUNT_CODES, completionHandler })

// Sagas

function* getCartSaga() {
  yield put({ type: CART_SET_FETCHING_CART })
  const { ok, data } = yield call(api.getCart, 0)
  if (ok)
    yield put({ type: CART_SET_CART, data })
  else
    yield put({ type: CART_SET_CART, data: {} })
}

function* updateCartSaga({ data }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data: cartData} = yield call(api.updateCart, 0, data)
  if (ok) {
    yield put({ type: CART_SET_CART, data: cartData })    
  }
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
}

function* addToCartSaga({ product, quantity, message }) {
  const lines = yield select(state => state.cart.data.lines || [])

  const line = lines.find(l => l.product.uid == product.uid)

  const data = {
    lines: [{
      product: { uid: product.uid },
      quantity: quantity + (line ? line.quantity : 0)
    }]
  }

  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data: cartData} = yield call(api.updateCart, 0, data)
  if (ok) {
    toast.info(message);
    yield put({ type: CART_SET_CART, data: cartData })    
  }
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
}

function* removeFromCartSaga({ product, quantity, message }) {
  const lines = yield select(state => state.cart.data.lines || [])

  const line = lines.find(l => l.product.uid == product.uid)
  if (!line || line.quantity <= 0)
    return

  const data = {
    lines: [{
      product: { uid: product.uid },
      quantity: line.quantity - quantity
    }]
  }

  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data: cartData} = yield call(api.updateCart, 0, data)
  if (ok){
    toast.info(message);
    yield put({ type: CART_SET_CART, data: cartData })    
  }
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
}

function* searchSubdistrictsSaga({ q, completionHandler }) {
  const { ok, data } = yield call(api.searchSubdistricts, q)
  completionHandler(ok, data)
}

function* getPaymentMethodsSaga({ completionHandler }) {
  const { ok, data } = yield call(api.getPaymentMethods, 0)
  completionHandler(ok, data)
}

function* getShippingMethodsSaga({ completionHandler }) {
  const { ok, data } = yield call(api.getShippingMethods, 0)
  completionHandler(ok, data)
}

function* updateContactSaga({ contact_data }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data } = yield call(api.updateCart, 0, { contact: contact_data })
  if (ok)
    yield put({ type: CART_SET_CART, data })    
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
}

function* updateShipToSaga({ ship_to, completionHandler }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data } = yield call(api.updateCart, 0, { ship_to })
  if (ok)
    yield put({ type: CART_SET_CART, data })    
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
  if (completionHandler)
    completionHandler(ok, data)
}

function* updatePaymentMethodSaga({ payment_method, message }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data } = yield call(api.updateCart, 0, { payment_method })
  if (ok){
    toast.info(message);
    yield put({ type: CART_SET_CART, data })    
  }
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
}

function* updateShippingMethodSaga({ shipping_method, message }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data } = yield call(api.updateCart, 0, { shipping_method })
  if (ok){
    toast.info(message);
    yield put({ type: CART_SET_CART, data })    
  }
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  
}

function* addDiscountSaga({ code, completionHandler }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data } = yield call(api.addDiscount, 0, code)
  if (ok)
    yield put({ type: CART_SET_CART, data })    
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  

  if (completionHandler)
    completionHandler(ok, data)
}

function* removeDiscountSaga({ code, completionHandler }) {
  yield put({ type: CART_SET_UPDATING_CART })
  const { ok, data } = yield call(api.removeDiscount, 0, code)
  if (ok)
    yield put({ type: CART_SET_CART, data })    
  else
    yield put({ type: CART_SET_UPDATING_CART, isUpdating: false })  

  if (completionHandler)
    completionHandler(ok, data)
}

function* paySaga({ callback_url, completionHandler }) {
  const { ok, data } = yield call(api.pay, 0, { callback_url })
  completionHandler(ok, data)
}

function* getDiscountCodesSaga({ completionHandler }) {
  const { ok, data } = yield call(api.getDiscountCodeList, 0)
  completionHandler(ok, data)
}

export function* cartSaga() {
  yield takeLatest(CART_ACTION_GET_CART, getCartSaga)
  yield takeEvery(CART_ACTION_UPDATE_CART, updateCartSaga)
  yield takeEvery(CART_ACTION_ADD_TO_CART, addToCartSaga)
  yield takeEvery(CART_ACTION_REMOVE_FROM_CART, removeFromCartSaga)
  yield takeEvery(CART_ACTION_SEARCH_SUBDISTRICTS, searchSubdistrictsSaga)
  yield takeEvery(CART_ACTION_GET_PAYMENT_METHODS, getPaymentMethodsSaga)
  yield takeEvery(CART_ACTION_GET_SHIPPING_METHODS, getShippingMethodsSaga)
  yield takeEvery(CART_ACTION_UPDATE_CONTACT, updateContactSaga)
  yield takeEvery(CART_ACTION_UPDATE_SHIP_TO, updateShipToSaga)
  yield takeEvery(CART_ACTION_UPDATE_SHIPPING_METHOD, updateShippingMethodSaga)    
  yield takeEvery(CART_ACTION_UPDATE_PAYMENT_METHOD, updatePaymentMethodSaga)
  yield takeEvery(CART_ACTION_PAY, paySaga)
  yield takeEvery(CART_ACTION_ADD_DISCOUNT, addDiscountSaga)
  yield takeEvery(CART_ACTION_REMOVE_DISCOUNT, removeDiscountSaga)
  yield takeEvery(CART_ACTION_GET_DISCOUNT_CODES, getDiscountCodesSaga)
}
