import {
  CONVERSATION_ACTION_WS_CONNECT,
  CONVERSATION_ACTION_WS_DISCONNECT,
  CONVERSATION_ACTION_WS_SEND,
  CONVERSATION_SET_WS_CONNECTING,
  CONVERSATION_SET_WS_CONNECTED,
  CONVERSATION_SET_WS_DISCONNECTED,
  CONVERSATION_RECEIVE_NEW_MESSAGE,
  CONVERSATION_RECEIVE_CONVERSATION_MESSAGES_UPDATE,
  CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
} from '@store/actions';

export const createConversationMiddleware = () => {
  let socket = null;

  const onOpen = store => (event) => {
    // console--.log('websocket open', event.target.url)
    store.dispatch({
      type: CONVERSATION_SET_WS_CONNECTED,
      host: event.target.url,
    });
  };

  const onClose = store => () => {
    // console--.log('websocket close');   
    socket = null;
    store.dispatch({
      type: CONVERSATION_SET_WS_DISCONNECTED,
    });
  };

  const onMessage = store => (event) => {
    const payload = JSON.parse(event.data);
    // if (dispatchEvent)
    //   dispatchEvent("message", payload)

    // console--.log("onMessage: ", payload.type, payload.data)

    switch (payload.type) {
      case 'new_message':
        store.dispatch({
          type: CONVERSATION_RECEIVE_NEW_MESSAGE,
          key: payload.data.conversation.uid,
          data: payload.data,
        });
        break;
      case 'messages_updated':
        //alert("onMessage: " + payload.type)
        store.dispatch({
          type: CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
          key: payload.data.conversation.uid,
          data: payload.data.conversation,
        });
        store.dispatch({
          type: CONVERSATION_RECEIVE_CONVERSATION_MESSAGES_UPDATE,
          key: payload.data.conversation.uid,
          data: payload.data.messages
        });
        break;
      case 'update_conversation':
        store.dispatch({
          type: CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
          key: payload.data.uid,
          data: payload.data,
        });
        break;
      default:
        break;
    }
  };

  // the middleware part of this function
  return store => next => action => {
    switch (action.type) {
      case CONVERSATION_ACTION_WS_CONNECT:
        if (socket !== null && !action.online) break;
        if (socket !== null && action.online) {
          if (action.chrome) {
            socket.close();
          }
          socket = new WebSocket(action.host);
          // console--.log('socket 22', socket)
          store.dispatch({
            type: CONVERSATION_SET_WS_CONNECTING,
            host: action.host
          });
          socket.onmessage = onMessage(store);
          socket.onclose = onClose(store);
          socket.onopen = onOpen(store);
          break;
        }

        // connect to the remote host
        socket = new WebSocket(action.host);

        store.dispatch({
          type: CONVERSATION_SET_WS_CONNECTING,
          host: action.host
        });
        // store.dispatch(wsStateChanged(WSSTATE.Connecting))

        // websocket handlers
        socket.onmessage = onMessage(store);
        socket.onclose = onClose(store);
        socket.onopen = onOpen(store);

        break;

      case CONVERSATION_ACTION_WS_DISCONNECT:
        if (socket !== null) {
          socket.close();
        }
        socket = null;
        // console--.log('websocket closed')
        break;

      // case 'WS_START':
      //   // Send start command

      // case 'WS_CONNECTED':
      //   store.dispatch(wsStateChanged(WSSTATE.Connected))
      //   break;

      // case 'WS_CONNECTING':
      //   store.dispatch(wsStateChanged(WSSTATE.Connecting))
      //   break;

      // case 'WS_DISCONNECTED':
      //   store.dispatch(wsStateChanged(WSSTATE.Disconnected))
      //   socket = null          
      //   break;

      case CONVERSATION_ACTION_WS_SEND:
        if (socket == null)
          break;
        // console--.log('sending a message: ', action.command)
        socket.send(JSON.stringify({ command: action.command, data: action.data }));
        break;

      default:
        // // console--.log('the next action:', action)
        return next(action);
    }
  };
};

export default createConversationMiddleware;