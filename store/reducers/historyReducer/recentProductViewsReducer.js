import {
  VISITOR_SET_FETCHING_RECENT_PRODUCT_VIEWS,
  VISITOR_SET_RECENT_PRODUCT_VIEWS,
} from '@store/actions'

import createReducer from '@lib/createReducer'
import moment from 'moment'

export const initialState = {
  isFetching: false,
  lastFetched: undefined,
  data: undefined
}

const setFetchingRecentProductViews = (state, { isFetching=true }) => ({ ...state, isFetching})
const setRecentProductViews = (state, { data }) => ({ 
  ...state, 
  isFetching: false, 
  data,
  lastFetched: new Date()
})

export const actionList = {
  [VISITOR_SET_FETCHING_RECENT_PRODUCT_VIEWS]: setFetchingRecentProductViews,
  [VISITOR_SET_RECENT_PRODUCT_VIEWS]: setRecentProductViews,
}

export const reducer = createReducer(initialState, actionList)
