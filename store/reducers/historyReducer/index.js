import * as recentProductViewsReducer from './recentProductViewsReducer'

const initialState = {
  recentProductViews: {},
}

export default (state = initialState, action) => {
  if (Object.keys(recentProductViewsReducer.actionList).includes(action.type)) {        
    return {
      ...state,
      recentProductViews: recentProductViewsReducer.reducer(state.recentProductViews, action),
    }
  }

  return state
}
