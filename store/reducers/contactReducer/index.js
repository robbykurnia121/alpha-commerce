import createReducer from '@lib/createReducer'
import {
  CONTACT_SET_FETCHING_CONTACT,
  CONTACT_SET_CONTACT,
  CONTACT_SET_UPDATING_CONTACT,
} from '@store/actions'

const initialState = {
  lastFetched: null,
  isFetching: false,
  isUpdating: false,
  data: null,
}

const setFetchingContact = (state, { isFetching=true }) => ({ ...state, isFetching})
const setUpdatingContact = (state, { isUpdating=true }) => ({ ...state, isUpdating})
const setContact = (state, { data }) => ({
  ...state, 
  isFetching: false,
  isUpdating: false,
  data,
  lastFetched: new Date().getTime(),
})

export default createReducer(initialState, {
  [CONTACT_SET_UPDATING_CONTACT]: setUpdatingContact,
  [CONTACT_SET_FETCHING_CONTACT]: setFetchingContact,
  [CONTACT_SET_CONTACT]: setContact,
})