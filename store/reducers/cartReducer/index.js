import createReducer from '@lib/createReducer'
import {
  CART_SET_FETCHING_CART,
  CART_SET_CART,
  CART_SET_UPDATING_CART,
} from '@store/actions'

const initialState = {
  lastFetched: null,
  isFetching: false,
  isUpdating: false,
  data: null,
}

const setFetchingCart = (state, { isFetching=true }) => ({ ...state, isFetching})
const setUpdatingCart = (state, { isUpdating=true }) => ({ ...state, isUpdating})
const setCart = (state, { data }) => ({
  ...state, 
  isFetching: false,
  isUpdating: false,
  data,
  lastFetched: new Date().getTime(),
})

export default createReducer(initialState, {
  [CART_SET_UPDATING_CART]: setUpdatingCart,
  [CART_SET_FETCHING_CART]: setFetchingCart,
  [CART_SET_CART]: setCart,
})