import createReducer from '@lib/createReducer'
import {
  ORDER_SET_FETCHING_ORDER_LIST,
  ORDER_SET_ORDER_LIST,
  ORDER_SET_ORDER_DETAIL,
} from '@store/actions'

const initialState = {
  isFetching: false,
  lastFetched: null,
  filter: {},
  sort: null,
  data: null,
}

const setFetchingOrderList = (state, { filter, isFetching=true }) => ({ ...state, filter, isFetching})
const setOrderList = (state, { filter, data }) => ({ 
  ...state, 
  isFetching: false, 
  filter,
  data,
  lastFetched: new Date().getTime(),
})

const setOrderDetail = (state, { data }) => {
  if (state.lastFetched) {
    return {
      ...state,
      data: state.data.map(r => {
        if (r.uid == data.uid)
          return data
        else
          return r
      })
    }
  }
  else
    return state
}

export const actionList = {
  [ORDER_SET_FETCHING_ORDER_LIST]: setFetchingOrderList,
  [ORDER_SET_ORDER_LIST]: setOrderList,
  [ORDER_SET_ORDER_DETAIL]: setOrderDetail,
}

export const reducer = createReducer(initialState, actionList)