import createReducer from '@lib/createReducer'
import {
  ORDER_SET_FETCHING_ORDER_DETAIL,
  ORDER_SET_ORDER_DETAIL,
  ORDER_SET_UPDATING_ORDER,
} from '@store/actions'

const initialState = {
  key: null,
  isFetching: false,
  isUpdating: false,
  lastFetched: null,
  data: null,
}

const setFetchingOrderDetail = (state, { isFetching=true }) => ({ ...state, isFetching})
const setUpdatingOrder = (state, { isUpdating=true }) => ({ ...state, isUpdating})
const setOrderDetail = (state, { data }) => ({ 
  ...state, 
  isFetching: false, 
  isUpdating: false,
  data,
  key: data.uid,
  lastFetched: new Date().getTime(),
})

export const actionList = {
  [ORDER_SET_FETCHING_ORDER_DETAIL]: setFetchingOrderDetail,
  [ORDER_SET_UPDATING_ORDER]: setUpdatingOrder,
  [ORDER_SET_ORDER_DETAIL]: setOrderDetail,
}

export const reducer = createReducer(initialState, actionList)