import createReducer from '@lib/createReducer'
import {
  ORDER_SET_FETCHING_SHIPPING_ADDRESS_LIST,
  ORDER_SET_SHIPPING_ADDRESS_LIST,
} from '@store/actions'

const initialState = {
  isFetching: false,
  lastFetched: null,
  filter: {},
  sort: null,
  data: null,
}

const setFetchingOrderList = (state, { filter, isFetching=true }) => ({ ...state, filter, isFetching})
const setOrderList = (state, { filter, data }) => ({ 
  ...state, 
  isFetching: false, 
  filter,
  data,
  lastFetched: new Date().getTime(),
})

export const actionList = {
  [ORDER_SET_FETCHING_SHIPPING_ADDRESS_LIST]: setFetchingOrderList,
  [ORDER_SET_SHIPPING_ADDRESS_LIST]: setOrderList,
}

export const reducer = createReducer(initialState, actionList)