import * as orderListReducer from './orderListReducer'
import * as orderDetailReducer from './orderDetailReducer'
import * as shippingAddressListReducer from './shippingAddressListReducer'
import {HYDRATE} from 'next-redux-wrapper'

const initialState = {
  lists: {},
  details: {},
  shippingAddressLists: {},
}

export function generateListKey(filter) {
  // ToDo: Add key by filter when we already support filter
  return "0"
}

export default (state = initialState, action) => {
  // 19 Apr 21. One action type now can be consumed by multiple reducers.
  // So we have to use newState to let each list update it.
  let newState = state

  if (Object.keys(orderListReducer.actionList).includes(action.type)) {        
    const listKey = generateListKey(action.filter, action.sort)
    newState = {
      ...newState,
      lists: {
        ...newState.lists,
        [listKey]: orderListReducer.reducer(newState.lists[listKey], action),
      }
    }
  }
  
  if (Object.keys(orderDetailReducer.actionList).includes(action.type)) {
    const key = action.key
    newState = {
      ...newState,
      details: {
        ...newState.details,
        [key]: orderDetailReducer.reducer(newState.details[key], action),
      }
    }
  }
  
  if (Object.keys(shippingAddressListReducer.actionList).includes(action.type)) {        
    const listKey = "0"
    newState = {
      ...newState,
      shippingAddressLists: {
        ...newState.shippingAddressLists,
        [listKey]: shippingAddressListReducer.reducer(newState.shippingAddressLists[listKey], action),
      }
    }
  }  
  
  return newState
}
