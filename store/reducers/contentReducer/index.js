import * as bannerReducer from './bannerReducer'
import {HYDRATE} from 'next-redux-wrapper'

const initialState = {
  banner: []
}

export default function ContentReducer (state = initialState, action) {
  if (action.type === HYDRATE) {
    return {
      ...state,
      banner: [...action.payload.content.banner]
    }
  } else if (Object.keys(bannerReducer.actionList).includes(action.type)) {
    return {
      ...state,
      banner: [
        ...Object.values(bannerReducer.reducer(state.banner, action))
      ]
    }
  }

  return state
}
