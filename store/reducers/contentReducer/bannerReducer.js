import createReducer from '@lib/createReducer'
import { CONTENT_SET_BANNER } from '@store/actions'

const initialState = []

const setHomeBanner = (state, { banner }) => ({ ...banner })

export const actionList = { [CONTENT_SET_BANNER]: setHomeBanner }

export const reducer = createReducer(initialState, actionList)
