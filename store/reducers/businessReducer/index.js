import createReducer from '@lib/createReducer'
import { BUSINESS_SET_DETAIL } from '@store/actions';
import { HYDRATE } from 'next-redux-wrapper';

const initialState = {
  isLoading: false,
  lastFetched: null,
};

const setBusinessDetail = (state, { data }) => ({
  ...state, 
  isLoading: false,
  lastFetched: new Date().getTime(),
  data
})

const hydrate = (state, { payload }) => ({ ...payload.business });

export default createReducer(initialState, {
  [BUSINESS_SET_DETAIL]: setBusinessDetail,
  [HYDRATE]: hydrate,
});