import * as conversationListReducer from './conversationListReducer'
import * as conversationDetailReducer from './conversationDetailReducer'
import * as statusReducer from './statusReducer'

const initialState = {
  lists: {},
  details: {},
  status: statusReducer.initialState,
}

export default (state = initialState, action) => {
  if (Object.keys(statusReducer.actionList).includes(action.type)) {        
    return {
      ...state,
      status: statusReducer.reducer(state.status, action),
    }
  }
  else if (Object.keys(conversationListReducer.actionList).includes(action.type)) {        
    const listKey = "0"
    return {
      ...state,
      lists: {
        ...state.lists,
        [listKey]: conversationListReducer.reducer(state.lists[listKey], action),
      }
    }
  }
  else if (Object.keys(conversationDetailReducer.actionList).includes(action.type)) {
    const key = action.key
    return {
      ...state,
      details: {
        ...state.details,
        [key]: conversationDetailReducer.reducer(state.details[key], action),
      }
    }
  }
  
  return state
}
