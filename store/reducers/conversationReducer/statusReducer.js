import createReducer from '@lib/createReducer'
import {
  WS_STATUS,
  CONVERSATION_SET_WS_CONNECTING,
  CONVERSATION_SET_WS_CONNECTED,
  CONVERSATION_SET_WS_DISCONNECTED,
} from '@store/actions'

export const initialState = {
  status: WS_STATUS.Idle,
  host: null,
}

const setConnecting = (state, { host }) => ({ ...state, status: WS_STATUS.Connecting, host })
const setConnected = (state, { host }) => ({ ...state, status: WS_STATUS.Connected, host })
const setDisconnected = (state) => ({ ...state, status: WS_STATUS.Disconnected })

export const actionList = {
  [CONVERSATION_SET_WS_CONNECTING]: setConnecting,
  [CONVERSATION_SET_WS_CONNECTED]: setConnected,
  [CONVERSATION_SET_WS_DISCONNECTED]: setDisconnected,
}

export const reducer = createReducer(initialState, actionList)