import {
  CONVERSATION_SET_FETCHING_CONVERSATION_LIST,
  CONVERSATION_SET_CONVERSATION_LIST,
  CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
} from '@store/actions'

import { parseConversationData, parseMessageBody } from './conversationDetailReducer'
import createReducer from '@lib/createReducer'
import moment from 'moment'

export const initialState = {
  isFetching: false,
  lastFetched: undefined,
  data: undefined
}

const setFetchingConversationList = (state, { isFetching=true }) => ({ ...state, isFetching})
const setConversationList = (state, { data }) => ({ 
  ...state, 
  isFetching: false, 
  data: {
  	...data,
  	records: data.records.map(r => parseConversationData(r)),
  },
  lastFetched: new Date()
})

const receiveConversationUpdate = (state, { data, remove }) => {
  if (remove && !state.data.records.find(m => m.uid == data.uid))
    return state

  const newRecord = parseConversationData(data)

  const existingRecords = state.lastFetched ?
    state.data.records.filter(m => m.uid != newRecord.uid)
    : []

  let records
  if (remove)
    records = existingRecords
  else
    records = [...existingRecords, newRecord].sort(
      (a, b) => moment(b.last_active).toDate().getTime() - moment(a.last_active).toDate().getTime()
    )

  // let unread_count = 1
  // if (state.messages.data && state.messages.data.unread_count !== undefined)
  //   unread_count = state.data.unread_count + 1

  return {
    ...state,
    lastFetched: new Date().getTime(),
    data: {
      ...state.data,
      records
    }
  }            
}

export const actionList = {
  [CONVERSATION_SET_FETCHING_CONVERSATION_LIST]: setFetchingConversationList,
  [CONVERSATION_SET_CONVERSATION_LIST]: setConversationList,
  [CONVERSATION_RECEIVE_CONVERSATION_UPDATE]: receiveConversationUpdate,
}

export const reducer = createReducer(initialState, actionList)
