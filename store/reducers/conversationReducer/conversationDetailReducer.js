import {
  CONVERSATION_SET_FETCHING_CONVERSATION_DETAIL,
  CONVERSATION_SET_CONVERSATION_DETAIL,
  CONVERSATION_SET_FETCHING_CONVERSATION_MESSAGES,
  CONVERSATION_SET_CONVERSATION_MESSAGES,
  CONVERSATION_SET_SENDING_MESSAGE,
  CONVERSATION_RECEIVE_SEND_MESSAGE_RESULT,
  CONVERSATION_RECEIVE_NEW_MESSAGE,
  CONVERSATION_RECEIVE_CONVERSATION_UPDATE,
  CONVERSATION_RECEIVE_CONVERSATION_MESSAGES_UPDATE,
} from '@store/actions'
import createReducer from '@lib/createReducer'
import moment from 'moment'

const initialState = {
  isFetching: false,
  lastFetched: undefined,
  isUpdating: false,
  key: undefined,
  data: {},
  messages: {
    isFetching: false,    
    lastFetched: undefined,
    data: [],
  },
  pendingPosts: [],
}

const initialPostState = {
  isSending: false,
  key: undefined,
  data: undefined,
}

export function parseMessageBody(body) {
  if (body && body.startsWith("{") && body.endsWith("}")) {
    let data = JSON.parse(body)
    if (data['input_type']) {
      data.componentType = data['input_type'] + "_input"
      data.isInputComponent = true
    }
    else if (data['card_list'] )
      data.componentType = 'card_list'
    else if (data['card'])
      data.componentType = 'card'
    else
      data.componentType = null

    return data
  }
  else
    return {
      text: body || ""
    }
}

export function parseConversationData(data) {
  return {
    ...data,
    last_message: data.last_message && { 
      ...data.last_message, body: parseMessageBody(data.last_message.body)
    },
  }
}

const setFetchingConversationDetail = (state, { isFetching=true }) => ({ 
  ...state, 
  isFetching,
})

const setConversationDetail = (state, { data }) => ({ 
  ...state, 
  isFetching: false, 
  isUpdating: false,
  key: data.uid,
  data: parseConversationData(data),
  lastFetched: new Date(),
})

const receiveConversationUpdate = (state, { data }) => ({ 
  ...state, 
  key: data.uid,
  data: parseConversationData(data),
  lastFetched: new Date().getTime(),
})

const setFetchingConversationMessages = (state, { isFetching=true }) => ({
  ...state, 
  messages: {
    ...state.messages,
    isFetching,
  },
})

const setConversationMessages = (state, { data }) => ({
  ...state,
  messages: {
    isFetching: false, 
    data: {
      ...data,
      records: data.records && data.records.map(r => ({
        ...r,
        body: parseMessageBody(r.body)
      }))
    },
    lastFetched: new Date()
  },
})

const setSendingMessage = (state, { postKey, isSending=true, data }) => {

  const post = state.pendingPosts.find(p => p.key == postKey)
  if (post)
    return {
      ...state,
      pendingPosts: state.pendingPosts.map(p => {
        if (p.key == postKey)
          return {
            ...p,
            isSending,
            data: data || p.data,
          }
        return p
      })
    }
  else
    return {
      ...state,
      pendingPosts: [
        ...state.pendingPosts,
        {
          ...initialPostState,
          key: postKey,
          isSending,
          data: data || {},
        }
      ]
    }
}

const receiveSendMessageResult = (state, { postKey, data }) => {
  // Remove from pending posts
  const pendingPosts = state.pendingPosts.filter(p => p.key != postKey)
  // Add to message list
  const newRecord = {
    ...data,
    body: parseMessageBody(data.body)
  }

  // Handle if we already got sent message push from WS
  if (state.messages.lastFetched && state.messages.data.records.find(m => m.id == newRecord.id))
    // Skip it.
    return {
      ...state,
      pendingPosts,
    }

  // const existingRecords = state.messages.lastFetched ?
  //   state.messages.data.records.filter(m => m.id != newRecord.id)
  //   : []
  const existingRecords = state.messages.lastFetched ? state.messages.data.records : []
  const records = [...existingRecords, newRecord].sort(
    (a, b) => moment(a.sent_at).toDate().getTime() - moment(b.sent_at).toDate().getTime()
  )

  return {
    ...state,
    pendingPosts,
    messages: {
      ...state.messages,
      lastFetched: new Date(),
      data: {
        ...state.messages.data,
        records
      }
    },
  }            
}

const receiveNewMessage = (state, { data }) => {
  const newRecord = {
    ...data,
    body: parseMessageBody(data.body)
  }

  const existingRecords = state.messages.lastFetched ?
    state.messages.data.records.filter(m => m.id != newRecord.id)
    : []
  const records = [...existingRecords, newRecord].sort(
    (a, b) => moment(a.sent_at).toDate().getTime() - moment(b.sent_at).toDate().getTime()
  )

  return {
    ...state,
    messages: {
      ...state.messages,
      lastFetched: new Date().getTime(),
      data: {
        ...state.messages.data,
        records
      }
    },
  }            
}

const receiveMessagesUpdate = (state, { data }) => {
  const newRecords = data.map(message => ({
    ...message,
    body: parseMessageBody(message.body),
  }))

  const existingRecords = state.messages.lastFetched ?
    state.messages.data.records.filter(m => !newRecords.find(x => x.id == m.id))
    : []

  const records = [...existingRecords, ...newRecords].sort(
    (a, b) => moment(a.sent_at).toDate().getTime() - moment(b.sent_at).toDate().getTime()
  )

  return {
    ...state,
    messages: {
      ...state.messages,
      lastFetched: new Date().getTime(),
      data: {
        ...state.messages.data,
        records
      }
    },
  }            
}

export const actionList = {
  [CONVERSATION_SET_FETCHING_CONVERSATION_DETAIL]: setFetchingConversationDetail,
  [CONVERSATION_SET_CONVERSATION_DETAIL]: setConversationDetail,
  [CONVERSATION_SET_FETCHING_CONVERSATION_MESSAGES]: setFetchingConversationMessages,
  [CONVERSATION_SET_CONVERSATION_MESSAGES]: setConversationMessages,
  [CONVERSATION_SET_SENDING_MESSAGE]: setSendingMessage,
  [CONVERSATION_RECEIVE_SEND_MESSAGE_RESULT]: receiveSendMessageResult,
  [CONVERSATION_RECEIVE_NEW_MESSAGE]: receiveNewMessage,
  [CONVERSATION_RECEIVE_CONVERSATION_UPDATE]: receiveConversationUpdate,
  [CONVERSATION_RECEIVE_CONVERSATION_MESSAGES_UPDATE]: receiveMessagesUpdate
}

export const reducer = createReducer(initialState, actionList)