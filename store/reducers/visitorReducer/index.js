import createReducer from '@lib/createReducer'
import {
  VISITOR_SET_INITIALIZING,
  VISITOR_SET_INIT_RESULT,
  VISITOR_SET_REGISTERING,
  VISITOR_SET_REGISTER_RESULT,
} from '@store/actions'

const initialState = {
  isInitializing: false,
  initResult: null,
  isRegistering: false,
  registerResult: null,
  isReady: false,
}

const setInitializing = (state, { isInitializing=true, isReady=false }) => ({ ...state, isInitializing, isReady })
const setInitResult = (state, { result }) => ({
  ...state, 
  isInitializing: false, 
  initResult: result,
  initTime: new Date().getTime(),
})
const setRegistering = (state, { isRegistering=true }) => ({ ...state, isRegistering})
const setRegisterResult = (state, { result }) => ({
  ...state, 
  isRegistering: false, 
  registerResult: result,
  registerTime: new Date().getTime(),
  isReady: true,
})

export default createReducer(initialState, {
  [VISITOR_SET_INITIALIZING]: setInitializing,
  [VISITOR_SET_INIT_RESULT]: setInitResult,
  [VISITOR_SET_REGISTERING]: setRegistering,
  [VISITOR_SET_REGISTER_RESULT]: setRegisterResult,  
})