import { combineReducers } from 'redux'

import visitor from './visitorReducer'
import auth from './authReducer'
import business from './businessReducer'
import product from './productReducer'
import cart from './cartReducer'
import order from './orderReducer'
import contact from './contactReducer'
import conversation from './conversationReducer'
import referral from './referralReducer'
import rating from './productReducer/productRatingReducer'
import history from './historyReducer'
import content from './contentReducer'

const reducers = {
  visitor,
  auth,
  business,
  contact,
  product,
  cart,
  order,
  conversation,
  referral,
  rating,
  history,
  content
}

export default combineReducers(reducers)