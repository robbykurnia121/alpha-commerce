import createReducer from '@lib/createReducer'
import { REFERRAL_SET_CODE } from '@store/actions'

const initialState = { referral_code: null }

const setReferralCode = (state, { referral }) => ({ ...state, ...referral })

export default createReducer(initialState, {
  [REFERRAL_SET_CODE]: setReferralCode
})
