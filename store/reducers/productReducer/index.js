import * as productListReducer from './productListReducer';
import * as productDetailReducer from './productDetailReducer';
import * as flashSaleListReducer from './flashSaleListReducer';
import { HYDRATE } from 'next-redux-wrapper';

const initialState = {
  lists: {},
  details: {},
  ratings: {},
  flashSaleLists: {},
};

export function generateListKey(filter, sort) {
  let path = "/";
  if (filter?.category)
    path += filter.category;
  let brand = filter?.brand || "";
  let flash_sale = filter?.flash_sale || "";
  let view = filter?.view || "";

  return `${path}?brand=${brand}&flash_sale=${flash_sale}&view=${view}&sort={sort}`;
}

export default (state = initialState, action) => {
  // console--.log(action.type)
  if (action.type === HYDRATE) {
    // // console--.log("Product State:", state)
    // // console--.log("Payload:", action.payload.product)
    return {
      ...state,
      lists: {
        ...state.lists,
        ...action.payload.product.lists,
      },
      details: {
        ...state.details,
        ...action.payload.product.details,
      },
      flashSaleLists: {
        ...state.flashSaleLists,
        ...action.payload.product.flashSaleLists
      }
    };
  }
  else if (Object.keys(productListReducer.actionList).includes(action.type)) {
    const listKey = generateListKey(action.filter, action.sort);
    return {
      ...state,
      lists: {
        ...state.lists,
        [listKey]: productListReducer.reducer(state.lists[listKey], action),
      }
    };
  }
  else if (Object.keys(productDetailReducer.actionList).includes(action.type)) {
    const key = action.key;
    return {
      ...state,
      details: {
        ...state.details,
        [key]: productDetailReducer.reducer(state.details[key], action),
      }
    };
  }
  else if (Object.keys(flashSaleListReducer.actionList).includes(action.type)) {
    const listKey = "0";
    return {
      ...state,
      flashSaleLists: {
        ...state.flashSaleLists,
        [listKey]: flashSaleListReducer.reducer(state.flashSaleLists[listKey], action),
      }
    };
  }

  return state;
};
