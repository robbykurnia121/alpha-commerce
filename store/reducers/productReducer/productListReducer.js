import createReducer from '@lib/createReducer'
import {
  PRODUCT_SET_FETCHING_PRODUCT_LIST,
  PRODUCT_SET_PRODUCT_LIST,
} from '@store/actions'

const initialState = {
  isFetching: false,
  lastFetched: null,
  filter: {},
  sort: null,
  data: null,
}

const setFetchingProductList = (state, { filter, sort, isFetching=true }) => ({ ...state, filter, sort, isFetching})
const setProductList = (state, { filter, sort, data }) => ({ 
  ...state, 
  isFetching: false, 
  filter,
  sort,
  data,
  lastFetched: new Date().getTime(),
})

export const actionList = {
  [PRODUCT_SET_FETCHING_PRODUCT_LIST]: setFetchingProductList,
  [PRODUCT_SET_PRODUCT_LIST]: setProductList,
}

export const reducer = createReducer(initialState, actionList)