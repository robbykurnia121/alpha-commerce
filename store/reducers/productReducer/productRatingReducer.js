import createReducer from '@lib/createReducer'
import { PRODUCT_SET_PRODUCT_RATING } from '@store/actions'

const initialState = {
  data: [],
}

const setProductRating = (state, { data }) => ({ 
  ...state, 
  data,
})

export default createReducer(initialState, {
  [PRODUCT_SET_PRODUCT_RATING]: setProductRating
})