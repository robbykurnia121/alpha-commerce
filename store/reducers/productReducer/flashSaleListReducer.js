import createReducer from '@lib/createReducer'
import {
  PRODUCT_SET_FETCHING_FLASH_SALE_LIST,
  PRODUCT_SET_FLASH_SALE_LIST,
} from '@store/actions'

const initialState = {
  isFetching: false,
  lastFetched: null,
  data: null,
}

const setFetchingFlashSaleList = (state, { isFetching=true }) => ({ ...state, isFetching})
const setFlashSaleList = (state, { data }) => ({ 
  ...state, 
  isFetching: false, 
  data,
  lastFetched: new Date().getTime(),
})

export const actionList = {
  [PRODUCT_SET_FETCHING_FLASH_SALE_LIST]: setFetchingFlashSaleList,
  [PRODUCT_SET_FLASH_SALE_LIST]: setFlashSaleList,
}

export const reducer = createReducer(initialState, actionList)