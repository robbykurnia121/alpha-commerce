import createReducer from '@lib/createReducer'
import {
  PRODUCT_SET_FETCHING_PRODUCT_DETAIL,
  PRODUCT_SET_PRODUCT_DETAIL,
} from '@store/actions'

const initialState = {
  key: null,
  isFetching: false,
  lastFetched: null,
  data: null,
}

const setFetchingProductDetail = (state, { isFetching=true }) => ({ ...state, isFetching})
const setProductDetail = (state, { data }) => ({ 
  ...state, 
  isFetching: false, 
  data,
  key: data.uid,
  lastFetched: new Date().getTime(),
})

export const actionList = {
  [PRODUCT_SET_FETCHING_PRODUCT_DETAIL]: setFetchingProductDetail,
  [PRODUCT_SET_PRODUCT_DETAIL]: setProductDetail,
}

export const reducer = createReducer(initialState, actionList)