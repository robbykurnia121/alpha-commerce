import createReducer from '@lib/createReducer'
import {
  AUTH_SET_INITIALIZING,
  AUTH_SET_NOT_LOGGED_IN,
  AUTH_SET_USER_INFO,
  AUTH_SET_TRIGGER_PROFILE
} from '@store/actions'

const initialState = {
  isIntializing: false,
  isReady: false,
  isLoggedIn: false,
  userInfo: {},
  triggerProfileModal: false,
}

const setInitializing = (state, { isIntializing }) => ({
  ...state,
  isIntializing,
})

const setNotLoggedIn = (state) => ({
  ...state,
  isInitializing: false,
  isLoggedIn: false,
  isReady: true,
  userInfo: {},
})

const setUserInfo = (state, { userInfo, authToken }) => ({
  ...state, 
  isInitializing: false,
  isLoggedIn: true,
  isReady: true,
  userInfo,
  authToken,
})

const setTriggerProfileModal = (state, { triggerProfileModal }) => ({
  ...state,
  triggerProfileModal
})

export default createReducer(initialState, {
  [AUTH_SET_USER_INFO]: setUserInfo,
  [AUTH_SET_INITIALIZING]: setInitializing,
  [AUTH_SET_NOT_LOGGED_IN]: setNotLoggedIn,
  [AUTH_SET_TRIGGER_PROFILE]: setTriggerProfileModal,
})