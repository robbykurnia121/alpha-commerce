import configureStore from "./configureStore"
import { createWrapper } from 'next-redux-wrapper'

// export const { store, eventSource } = configureStore()
// export const store = configureStore()
// export default store

export const storeWrapper = createWrapper(configureStore, { debug: true, storeKey: "alpha_store" })
