import { useSelector } from 'react-redux'
import { generateListKey } from '../reducers/orderReducer'

export const useOrderList = (filter) => (
  useSelector(state => state.order.lists[generateListKey(filter)] || {})
)
export const useOrderDetail = (key) => (
  useSelector(state => state.order.details[key] || {})
)
export const useShippingAddressList = () => (
  useSelector(state => state.order.shippingAddressLists["0"] || {})
)