import { useSelector } from 'react-redux'

export const useRecentProductViews = () => (
  useSelector(state => state.history.recentProductViews)
)