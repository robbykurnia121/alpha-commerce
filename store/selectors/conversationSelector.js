import { useSelector } from 'react-redux'

export { WS_STATUS } from '../actions/conversationAction'

export const useWSStatus = () => (
  useSelector(state => state.conversation.status)
)

export const useConversationList = (listKey="0") => (
  useSelector(state => state.conversation.lists[listKey] || {})
)

export const useConversationDetail = (key) => (
  useSelector(state => state.conversation.details[key] || {})
)