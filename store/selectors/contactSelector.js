import { useSelector } from 'react-redux'

export const useContact = () => (
  useSelector(state => state.contact)
)