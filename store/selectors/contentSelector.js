import { useSelector } from 'react-redux'

export const useContent = () => (
  useSelector(state => state.content)
)

export const useHomeBanner = () => (
  useSelector(state => state.content.banner)
)