import { useSelector } from 'react-redux'
import { generateListKey } from '../reducers/productReducer'

export const useProductList = (filter, sort) => (
  useSelector(state => state.product.lists[generateListKey(filter, sort)] || {})
)

export const useProductDetail = (key) => (
  useSelector(state => state.product.details[key] || {})
)

export const useFlashSaleList = () => (
  useSelector(state => state.product.flashSaleLists["0"] || {})
)

export const useProductRatings = () => (
  useSelector(state => state.rating)
)