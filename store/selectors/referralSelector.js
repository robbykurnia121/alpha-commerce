import { useSelector } from 'react-redux'

export const useReferral = () => (
  useSelector(state => state.referral)
)