import { useSelector } from 'react-redux'

export const useBusinessDetail = () => (
  useSelector(state => state.business)
);  