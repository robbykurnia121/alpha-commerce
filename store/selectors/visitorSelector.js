import { useSelector } from 'react-redux'

export const useVisitor = () => (
  useSelector(state => state.visitor)
)