import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './reducers'
import rootSaga from './actions/sagas'

import { createConversationMiddleware } from './middlewares/conversationMiddleware'

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension')
    return composeWithDevTools(applyMiddleware(...middleware))
  }
  return applyMiddleware(...middleware)
}

export default (initialState) => {
  const middlewares = []
  const sagaMiddleware = createSagaMiddleware()
  const conversationMiddleware = createConversationMiddleware()
  middlewares.push(sagaMiddleware)
  middlewares.push(conversationMiddleware)

  const store = createStore(
  	rootReducer, 
  	initialState, 
  	bindMiddleware(middlewares)
  )

  store.sagaTask = sagaMiddleware.run(rootSaga)

  return store
  // return { store, eventSource }
}
