import '@assets/styles/globals.css';
import Router from 'next/router';
import NProgress from 'nprogress';
import { STORE_API_URL, STORE_API_KEY } from '@configs/keys';
import { useEffect, useState } from 'react';
import { storeWrapper } from '@store';
import { useDispatch, register, authInit, getContact, wsConnect, getConversationList, CONVERSATION_SET_WS_DISCONNECTED } from '@store/actions';
import { useAuth, useVisitor, useWSStatus, WS_STATUS } from '@store/selectors';
import 'react-image-lightbox/style.css';

// ---------- NProgress loader config and events start ---------- //

import "nprogress/nprogress.css";

Router.onRouteChangeStart = () => {
  NProgress.configure({ showSpinner: false }).start();
};
Router.onRouteChangeComplete = () => {
  NProgress.done();
};
Router.onRouteChangeError = () => {
  NProgress.done();
};

// ---------- NProgress loader config and events end ---------- //

const Noop = ({ children }) => <>{children}</>;

function App({ Component, pageProps }) {
  const Layout = Component.Layout || Noop;
  const dispatch = useDispatch();
  const auth = useAuth();
  const visitor = useVisitor();
  const wsStatus = useWSStatus().status;

  useEffect(() => {
    if (typeof window !== 'undefined') {
      dispatch(authInit());
    }
  }, []);

  useEffect(() => {
    if (typeof window !== 'undefined' && auth.isReady) {
      if (auth.isLoggedIn)
        dispatch(register({
          external_app: 'alpha.apps.directory',
          external_model: 'user',
          external_id: auth.userInfo.uid,
          first_name: auth.userInfo.first_name,
          last_name: auth.userInfo.last_name,
          email: auth.userInfo.email,
          mobile_no: auth.userInfo.mobile_no,
        }));
      else
        dispatch(register());
    }
  }, [auth.isReady, auth.isLoggedIn]);

  useEffect(() => {
    if (visitor.isReady)
      dispatch(getContact());
  }, [visitor.isReady]);

  const [isOnline, setIsOnline] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      window.addEventListener('online', (e) => {
        setIsOnline(true);
        // alert("Online")
      });

      window.addEventListener('offline', (e) => {
        setIsOnline(false);
        // alert("offline")
        dispatch({ type: CONVERSATION_SET_WS_DISCONNECTED });
      });
    }

    return () => {
    };
  }, []);

  useEffect(() => {
    const isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);
    if (!visitor.isReady)
      return;

    if (wsStatus === WS_STATUS.Idle || wsStatus === WS_STATUS.Disconnected) {
      let timeout = 250;
      if (!isOnline && wsStatus === WS_STATUS.Disconnected)
        timeout = 3000;

      const getWSHost = () => {
        let host = STORE_API_URL + "/ws/conversation/";
        host = host.replace("http://", "ws://");
        host = host.replace("https://", "wss://");
        host += "?visitor=" + visitor?.registerResult?.visitor_id;

        if (auth.isLoggedIn)
          host += "&token=" + auth.authToken;

        // Must add api key for client
        host += "&api_key=" + STORE_API_KEY;

        return host;
      };

      setTimeout(() => {
        dispatch(wsConnect(getWSHost(), isOnline, isChrome));
      }, timeout);
    }
    else if (wsStatus == WS_STATUS.Connected) {
      // Refresh conversation list on re-connect.
      dispatch(getConversationList());
    }
  }, [visitor.isReady, wsStatus, isOnline]);

  useEffect(() => {
    // console--.log("wsStatus:", wsStatus);
  }, [wsStatus]);

  return (
    <>
      <style jsx global>
        {`
        #nprogress .bar {
          background: rgb(235, 205, 47);
        }
        #nprogress .peg {
          box-shadow: 0 0 10px rgb(235, 205, 47), 0 0 5px rgb(235, 205, 47);
        }
      `}
      </style>
      <Layout pageProps={pageProps}>
        <Component {...pageProps} />
      </Layout>
    </>
  );

  return <Component {...pageProps} />;
}

export default storeWrapper.withRedux(App);
