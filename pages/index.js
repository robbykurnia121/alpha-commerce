import React, { useEffect, Fragment } from 'react';
import { END } from 'redux-saga';
import { storeWrapper } from '@store';
import * as api from '@api/product';
import {
  getProductList,
  getBusinessDetail,
  getFlashSaleList,
  getHomeBannerList,
  useDispatch,
} from '@store/actions';
import {
  useProductList,
  useFlashSaleList,
  useHomeBanner
} from '@store/selectors';
import { useSelector } from 'react-redux';
import NextLink from 'next/link';
import Image from 'next/image';
import { Slider, Text } from 'alpha-ui-goapp';
import { ELECTRONIC, COSMETIC, HEALTHCARE, LIFESTYLE } from '@configs/rootCategories';
import { POPULAR, BEST_SELLER } from '@configs/collectionTypes';
import { DIGITAL_STORE_URL } from '@configs/keys';
// import { useUI } from '@components/ui/context';
import digitalProductTypes from '@configs/digitalProductTypes';

const Link = ({ href, children, ...props }) => {
  return (
    <NextLink href={href}>
      <a {...props}>{children}</a>
    </NextLink>
  );
};


export const getStaticProps = storeWrapper.getStaticProps(async ({ store }) => {
  let statusCode;
  const categoryList = [];
  const { data } = await api.getProductList();
  categoryList.push(...data.categories.map(brand => brand.key));
  categoryList.map(category => store.dispatch(getProductList({ category })));
  store.dispatch(getHomeBannerList());
  store.dispatch(getBusinessDetail());
  store.dispatch(getProductList());
  store.dispatch(getProductList({ view: BEST_SELLER.key }));
  store.dispatch(getProductList({ view: POPULAR.key }));

  // if (!store.getState().placeholderData) {
  // store.dispatch(loadData())
  store.dispatch(END);
  // }

  await store.sagaTask.toPromise();
  const productList = store.getState().product.lists;
  const homeBannerList = store.getState().content.banner;

  for (const [key, value] of Object.entries(productList)) {
    if (!value.lastFetched) {
      statusCode = 500;
      console.error(`Fail fetch ${key}`);
    }
  }

  if (homeBannerList.length === 0) {
    statusCode = 500;
    console.error(`Fail fetch Home Banner`);
  }

  if (statusCode) {
    return {
      props: { statusCode },
      revalidate: 1,
    };
  }

  return {
    props: { categoryList },
    revalidate: 180,
  };
});

export default function Home({ categoryList }) {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.product.lists) || {};
  const productList = useProductList();
  const banner = useHomeBanner();
  const bestSellerProductList = useProductList({ view: BEST_SELLER.key });
  const popularProductList = useProductList({ view: POPULAR.key });
  const flashSaleList = useFlashSaleList();


  useEffect(() => {
    dispatch(getFlashSaleList());

  }, []);

  // Only for test alpha-ui styling
  return (
    <div>
      <Slider loop>
        {banner?.map(item => (
          <div key={`banner-${item.uid}`}>
            <Link href={item?.link_to || ''}>
              <Image
                className='object-cover'
                src={item?.image?.image_url}
                width={1080}
                height={720}
                alt=''
              />
            </Link>
          </div>
        ))}
      </Slider>

      <Slider slidesPerView={5} spaceBetween={0}>
        {productList?.data?.brands?.map((item, index) => (
          <div key={`brand-${index}`}>
            <Link href={`/brand/${item.key}`} prefetch={false}>
              <Image
                quality="85"
                width={64}
                height={32}
                src={item?.image?.image_url}
                alt={item.name || 'Brand Image'}
                className='object-contain'
              />
            </Link>
          </div>
        ))}
      </Slider>

      {Object.values(categories)?.length > 0 && Object.values(categories).map((category, index) => (
        <div key={index} className='p-2'>
          {categoryList.includes(category?.filter?.category) && category.data?.categories?.length > 0 && (
            <Fragment>
              <div className='flex justify-between'>
                <Text className='font-bold'>{category.data.category.name}</Text>
                <Link href={`/category/${category.data.category.key}`} prefetch={false}><Text color='grey' size='xs'>Lihat Semua →</Text></Link >
              </div>
              <div className="py-1" />
              <Slider slidesPerView={3.5} spaceBetween={8}>
                {category.data.categories.map((item, index) => (
                  <div key={`brand-${index}`}>
                    <Link href={`/category/${category.data.category.key}/${item.key}`} prefetch={false}>
                      <Image
                        quality="85"
                        width={256}
                        height={256}
                        src={item?.image?.image_url}
                        alt={item.name || 'Category Image'}
                        className='object-cover'
                      />
                      <Text color='grey' size='xs'>{item.name}</Text>
                    </Link>
                  </div>
                ))}
              </Slider>
            </Fragment>
          )}
        </div>
      ))}

    </div>
  );
}

